var map;
var marker;
var geocoder;

function initializeMaps() {
  var mapOptions = {
    center: new google.maps.LatLng(-33.8688, 151.2195),
    zoom: 17,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  
   map = new google.maps.Map(document.getElementById('map_canvas'),mapOptions);

   marker = new google.maps.Marker({
       map: map,
       title:"Current Location",
       draggable:true
     });

  
   
   google.maps.event.addListener(marker, 'position_changed', showCurrentPos);

  var input = document.getElementById('address');
  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.bindTo('bounds', map);

  var infowindow = new google.maps.InfoWindow({disableAutoPan: false});
 
  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    infowindow.close();
    var place = autocomplete.getPlace();
    if (!place.geometry) {
        // Inform the user that the place was not found and return.
      //  alert("location not found")
    	showCurLoc();
        return;
      }
    
    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);  
    }

   /* var image = new google.maps.MarkerImage(
        place.icon,
        new google.maps.Size(71, 71),
        new google.maps.Point(0, 0),
        new google.maps.Point(17, 34),
        new google.maps.Size(35, 35));
        */
  //  marker.setIcon(image);
    marker.setPosition(place.geometry.location);

    var address = '';
    if (place.address_components) {
      address = [
        (place.address_components[0] && place.address_components[0].short_name || ''),
        (place.address_components[1] && place.address_components[1].short_name || ''),
        (place.address_components[2] && place.address_components[2].short_name || '')
      ].join(' ');
    }

    infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
    infowindow.open(map, marker);
  });

  
  google.maps.event.addListener(map, 'click', function(evt) {
	  
	  marker.setPosition(evt.latLng);
		map.setCenter(evt.latLng);
	map.panTo(evt.latLng);
	
  });
}


function showCurLoc(){
	
	 
navigator.geolocation.getCurrentPosition(
	      onSuccess,
	      onError, {
	        enableHighAccuracy: true,
	        timeout: 20000,
	        maximumAge: 120000
	      });
}

function showCurrentPos()
{
$("#lonid").val(marker.getPosition().lng());
$("#latid").val(marker.getPosition().lat());
}


function placeMarker( lat,  lon)
{
	var jsCenter = new google.maps.LatLng(lat, lon);
	marker.setPosition(jsCenter);
	map.setCenter(jsCenter);
map.panTo(jsCenter);
}

function onSuccess(position) {
    //the following are available to use
//position.coords.latitude
//position.coords.longitude
// position.coords.altitude
// position.coords.accuracy
// position.coords.altitudeAccuracy
// position.coords.heading
// position.coords.speed 
	console.log(" lat " + position.coords.latitude + " lon " +  position.coords.longitude)
placeMarker(position.coords.latitude,position.coords.longitude)
showCurrentPos();
//google.maps.event.trigger(map, 'resize');
}

function onError(position) {
//do something?
}