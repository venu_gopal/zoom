/* Bootstrap Carousel */

$('.carousel').carousel({
   interval: 8000,
   pause: "hover"
});

/* Navigation Menu */

ddlevelsmenu.setup("ddtopmenubar", "topbar");

/* Dropdown Select */

/* Navigation (Select box) */

// Create the dropdown base

$("<select />").appendTo(".navis");

// Create default option "Go to..."

$("<option />", {
   "selected": "selected",
   "value"   : "",
   "text"    : "Menu"
}).appendTo(".navis select");

// Populate dropdown with menu items

$(".navi a").each(function() {
 var el = $(this);
 $("<option />", {
     "value"   : el.attr("href"),
     "text"    : el.text()
 }).appendTo(".navis select");
});

$(".navis select").change(function() {
  window.location = $(this).find("option:selected").val();
});



/* Navigation (Select box) */

// Create the dropdown base

$("<select />").appendTo(".sub-navis");

// Create default option "Go to..."

$("<option />", {
   "selected": "selected",
   "value"   : "",
   "text"    : "Menu"
}).appendTo(".sub-select");

// Populate dropdown with menu items

$(".navi-cat a").each(function() {
 var el = $(this);
 $("<option />", {
     "value"   : el.attr("href"),
     "text"    : el.text()
 }).appendTo(".sub-navis select");
});

$(".navis select").change(function() {
  window.location = $(this).find("option:selected").val();
});









/* Scroll to Top */


  $(".totop").hide();

  $(function(){
    $(window).scroll(function(){
      if ($(this).scrollTop()>300)
      {
        $('.totop').fadeIn();
      } 
      else
      {
        $('.totop').fadeOut();
      }
    });

    $('.totop a').click(function (e) {
      e.preventDefault();
      $('body,html').animate({scrollTop: 0}, 500);
    });

  });
  
  
/* Support */

$("#slist a").click(function(e){
   e.preventDefault();
   $(this).next('p').toggle(200);
});

/* Careers */

$('#myTab a').click(function (e) {
  e.preventDefault()
  $(this).tab('show')
})


/* Ecommerce sidebar */

$(document).ready(function() {
    $('.sidey .nav').navgoco();
});


/* Load More  */

$(document).ready(function(){  
  $("#show").click(function(){
    $("#more").show(1000);
    $("#more2").show(2000);
	$("#show").hide();
  });
});

/* Login Form  */
$(document).ready(function(){  
  $("#login").click(function(){
     $("#loginbox").slideDown(1000);
		
  });
});


var haggel = angular.module('haggel',['satellizer']);

haggel.config(['$authProvider',
    function($authProvider) {
  
      // For any unmatched url, redirect to /state1
     
      $authProvider.loginSignup = true;
      $authProvider.loginRedirect = '/login.html';

      $authProvider.facebook({
        url: 'zoom/signIn/connectFacebook',
        clientId: '263863147021728',
        redirectUri: 'http://localhost:8080/login.html',
        scope: 'email',
        scopeDelimiter: ',',
        requiredUrlParams: ['display', 'scope'],  
      });

      $authProvider.google({
        url:"zoom/signIn/connectGoogle",
        clientId: '631036554609-v5hm2amv4pvico3asfi97f54sc51ji4o.apps.googleusercontent.com',
        redirectUri: 'http://localhost:8080/login.html',
      });

      $authProvider.github({
        clientId: '0ba2600b1dbdb756688b'
      });

      $authProvider.linkedin({
        clientId: '77cw786yignpzj'
      });

      $authProvider.twitter({
        url: '/auth/twitter',
        redirectUri: 'http://localhost:8080/login.html',
      });

      $authProvider.oauth2({
        name: 'foursquare',
        url: '/auth/foursquare',
        redirectUri: window.location.origin,
        clientId: 'MTCEJ3NGW2PNNB31WOSBFDSAD4MTHYVAZ1UKIULXZ2CVFC2K',
        authorizationEndpoint: 'https://foursquare.com/oauth2/authenticate',
      });
    }               
]);

haggel.controller('hgLoginCtrl', ['$scope', '$http', function ($scope, $http) {
    
    $scope.loginRequest = {};
    $scope.loginUser = function(isValid){
      
      if(isValid){
        $http.post('zoom',$scope.loginRequest).success(function(response){
          if(response.success){
            $('#loginpop').modal('hide');
          }
          else{
            alert(response.message);
          }
        });
      }
      else{
        $scope.submit = false;
      }
    };
  }
]);

haggel.controller('hgPassLoginCtrl', ['$scope','$http','$auth', '$location',
    function($scope,$http,$auth, $location) {  

    $scope.login = function() {
      $auth.login({ email: $scope.email, password: $scope.password })
        .then(function() {
          
        })
        .catch(function(response) {
          console.log(response.data.message);
        });
    };

        $scope.authenticate = function(provider) {
            
            $auth.authenticate(provider)
            .then(function(response) {
              console.log(response);
            });
        };

    }
]);


haggel.controller('hgRegisterCtrl', ['$scope', '$http', function ($scope, $http) {
    
    $scope.registerRequest = {};
    $scope.registerUser = function(isValid){
      
      if(isValid){
        $scope.submit = true;
        $http.post('zoom/shopper/doregister',$scope.registerRequest).success(function(response){
          if(response.success){
          }
          else{
            alert(response.message);
          }
        });
      }
      else{
        $scope.submit = false;
      }
    };
  }
]);