package com.luckydog.dealpost

import org.bson.types.ObjectId

class AdCategory {

    String name
    String description
    String imagePath
    Boolean isPopular = false
    String parentId = 0
    List<String> childCategory = new ArrayList()
    List<String> categoryPath = new ArrayList()
    int left
    int right


    static constraints = {
        imagePath blank: true, nullable: true
        description blank: true
        name blank: true

    }
    public static int getDefaultParentCategory (){
           return 1;
    }

}
