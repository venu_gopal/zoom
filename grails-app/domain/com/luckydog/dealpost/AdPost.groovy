package com.luckydog.dealpost

import com.luckydog.common.Address;
import com.luckydog.merchant.Merchant

class AdPost {

    String title
    String description
    String tac
    List location


    Float priorPrice = 0
    Float curPrice = 0
    Float percent = 0
    Date startDate = new Date()
    Date endDate
    Integer quantity = 1
    Date dateCreated
    Date lastUpdated
    Long karma = 0
    Float avgRating = 0.0
    Boolean isActive = true
//    List<String> categorypath
    List<AdCategory> adCategories
    List<Address> address
    static hasMany = [adCategories: AdCategory]
    static hasOne = [address: Address]
    static belongsTo = [merchant: Merchant]

    static embedded = ['adCategories']

    static transients = ['formattedEndDate', 'formattedStartDate']

    static constraints = {
        adCategories nullable: true
        tac nullable: true
    }


    static mapping = {
        location geoIndex: true
        //title index:true
        compoundIndex location: "2d", categories: 1
        description type: "text"
        tac type: "text"
    }


    public String toString() {
        "[$title]"
    }

    String getFormattedStartDate() {
        return this.startDate?.format('dd-MMM-yyyy')
    }

    String getFormattedEndDate() {
        return this.endDate?.format('dd-MMM-yyyy')
    }

}
