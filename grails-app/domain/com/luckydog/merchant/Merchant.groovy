package com.luckydog.merchant

import com.luckydog.common.Address;
import com.luckydog.common.CommonUser;
import com.luckydog.dealpost.AdPost
import com.luckydog.security.User
import com.luckydog.shopper.PurchaseHistory;

class Merchant extends User {

    String description

  //  static hasMany = [adposts: AdPost, address: Address]

    static constraints = {
      //  adposts nullable: true
		description nullable:true
	//	address nullable:true
    }

    static mapping = {
        description type: "text"
        //title index:true
        //compoundIndex location:"2d", categories:1
    }

    public String toString() {
        "[$name]"
    }
}
