package com.luckydog.shopper

import com.luckydog.dealpost.AdPost
import com.luckydog.security.User

class PurchaseHistory {

    Long buyerId
    Long itemId
    Integer status
    Date dateCreated
    Date dateUpdated

    static transients = ['adPost', 'buyer']

    AdPost getAdPost() {
        AdPost.findById(this.itemId)
    }

    User getBuyer() {
        User.get(this.buyerId)
    }
}
