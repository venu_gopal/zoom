package com.luckydog.shopper

import com.luckydog.security.User

import java.util.List;

import com.luckydog.common.Address;
import com.luckydog.common.CommonUser;
import com.luckydog.dealpost.AdPost;

/*
 * register: name, email, password, confirm password
 * profile: ^ + date of birth, gender, address (google maps kit) +  cur lat/lon
 */

class Shopper extends User {

    String uniqueId
    Integer age = 0
    String gender
    Date dateOfBirth

    static hasOne = [address: Address]

    static constraints = {
        gender nullable: true
        address nullable: true
        uniqueId nullable: true
        dateOfBirth nullable: true
    }
    //before saving set uniqueid to email if nothing exists
}
