package com.luckydog.security

import com.luckydog.shopper.PurchaseHistory
import com.luckydog.utils.social.user.SignUpCO
import org.bson.types.ObjectId

class User {

    transient springSecurityService
	ObjectId id
    String name
    String username
    String password
    boolean enabled
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired


    Date dateCreated
    Date lastUpdated

//    -----------------------------------

    Boolean hasVerifiedEmail = false
//    BigDecimal latitude
//    BigDecimal longitude
    Long karma = 0

    List<PurchaseHistory> boughtItems = []

    static hasMany = [boughtItems: PurchaseHistory]


    static constraints = {
        name blank: false, nullable: true
        username blank: false, email: true
        password blank: false
//        latitude(blank: true, nullable: true)
//        longitude(blank: true, nullable: true)
        karma(nullable: true)
    }

  
    User(SignUpCO signUpCO) {
        this.username = signUpCO.email
        this.name = signUpCO.name
        this.password = signUpCO.password
        this.enabled = true
    }

    static mapping = {
        password column: '`password`'
    }

    Set<Role> getAuthorities() {
        UserRole.findAllByUser(this).collect { it.role } as Set
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
//        password = springSecurityService.encodePassword(password)
    }
}
