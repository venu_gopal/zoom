package com.luckydog.common

import java.util.List;

import com.luckydog.location.City;
import com.luckydog.location.Country;

class Address {

    boolean isPrimary = false
    String email
    String fax
    String website
    String addressOne
    String addressTwo

    String state
    String postCode

    String description
    static hasMany = [phone: Phone]
    static hasOne = [country: Country, city: City]

    List location


    static constraints = {
        email nullable: true
        fax nullable: true
        website nullable: true
        addressOne nullable: true
        addressTwo nullable: true
        state nullable: true
        postCode nullable: true
        description nullable: true
    }

    static mapping = {
        location geoIndex: true
    }
}
