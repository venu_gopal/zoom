
import com.luckydog.dealpost.AdPost

class AdUpdaterJob  {

	def mongo

    def grailsApplication

	static triggers={
		cron name:"updater", cronExpression:'0 0/1 * * * ?'
	}


	/**
	 * Quartz requires a public empty constructor so that the
	 * scheduler can instantiate the class whenever it needs.
	 */
	public AdUpdaterJob() {
	}



	def execute() {

		log.info "Running Job"
		def db = mongo.getDB(grailsApplication.config.mongodb.databaseName)
		Date today = new Date()
		Date lastDay=today-180
		def critteria = AdPost.createCriteria()

		def listall= critteria.list {
			ge("dateCreated",lastDay)
			eq("isActive",true)
		}

		log.info "Processing $listall"
		for(def post in listall) {

			def adid= post.id
			def xoutput= db.karma.aggregate(['$match':[adPostId:adid, karmaType:2]],['$group':[_id:[ktype:'$karmaType'], average:[$avg:'$krating'], totalp:[$sum:1]]])
			def results= xoutput.results()
			log.debug "results for #: $adid is " + results
			float avgRating
			float totRating
			for(result in results) {
				avgRating=result.average
				totRating=result.totalp
			}



			if(avgRating>0){
				float karma= (((int)(10/Math.log(((today.time- post.dateCreated.time )/60000)+1))	) * ( (avgRating-2.5)+1 ) * Math.log(totRating+1) * 10) + 10*avgRating
				log.info "karma  for post # $adid is $karma"
				post.karma=karma
			}
			else {
				log.info "copy karma from merchant ${post.merchant} // ${post.merchant.karma}"
				post.karma= post.merchant.karma?:0
			}
			post.save()
		}
		/*
		 * def db = mongo.getDB(grailsApplication.config.mongodb.databaseName)
		 def xoutput= db.karma.aggregate(['$match':[adPostId:adid, karmaType:2]],['$group':[_id:[ktype:'$karmaType'], average:[$avg:'$krating'], totalp:[$sum:1]]])
		 def results= xoutput.results()
		 for(result in results)
		 {
		 post.avgRating=result.average
		 }
		 post.save()
		 */
	}
}
