<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="layout" content="homepage" />
	<title>Welcome to HAGGELL</title>

</head>
<body>
	
	

		
		<!-- Begin Main -->
		<div role="main" class="main">
			<!-- Begin Main Slide -->
			<section class="main-slide">
				<div class="container">
					<div id="owl-third-demo" class="owl-carousel main-demo second-demo">
						<div class="item"><img src="images/flatize/content/slides/home-slider1.jpg" alt="Photo">
							<div class="item-caption">
								<div class="item-caption-inner">
									<p class="item-cat"><a href="#">Fall/Winter 2014</a></p>
									<h2>Up to 50% off<br>on selected items</h2>
									<a href="#" class="btn btn-white">Shop Now</a>
								</div>
							</div>
						</div>
						<div class="item"><img src="images/flatize/content/slides/home-slider2.jpg" alt="Photo">
							<div class="item-caption">
								<div class="item-caption-inner">
									<p class="item-cat"><a href="#">Top</a></p>
									<h2>25% off<br>for pink swim</h2>
									<a href="#" class="btn btn-white">Shop Now</a>
								</div>
							</div>
						</div>
						<div class="item"><img src="images/flatize/content/slides/home-slider3.jpg" alt="Photo">
							<div class="item-caption">
								<div class="item-caption-inner">
									<p class="item-cat"><a href="#">Panties</a></p>
									<h2>Free shipping<br>on $50 plus $20</h2>
									<a href="#" class="btn btn-white">Shop Now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End Main Slide -->
			
			
			
			<!-- Begin About -->
			<section class="about">
				<div class="container">
					<div class="short-intro text-center">
						<p><strong>Personalized deals from your favourite merchants</strong></p>
						<h1>About Haggell</h1>
						<span class="divider bg-color"></span>
						<p> Get your favourite deals at your favourite prices. Let our system alert you for recommended stuff </p>
					</div>
					
					
					<div class="row row-narrow">
						<div class="col-sm-6">
							<div class="cat-item">
								<figure>
									<a href="shop-full-width.html"><img class="img-responsive" alt="" src="images/flatize/content/products/cat-1.jpg"></a>
									<figcaption>
										<div class="cat-description">
											<h3>Men’s Fashion and Style</h3>
											<a href="#">Read More</a>
										</div>
									</figcaption>
								</figure>
							</div>
						</div>
						<div class="col-xs-6 col-sm-3">
							<div class="cat-item">
								<figure>
									<a href="shop-full-width.html"><img class="img-responsive" alt="" src="images/flatize/content/products/cat-2.jpg"></a>
									<figcaption>
										<div class="cat-description">
											<h3>Men’s Fashion</h3>
											<a href="#">Read More</a>
										</div>
									</figcaption>
								</figure>
							</div>
						</div>
						<div class="col-xs-6 col-sm-3">
							<div class="cat-item">
								<figure>
									<a href="shop-full-width.html"><img class="img-responsive" alt="" src="images/flatize/content/products/cat-3.jpg"></a>
									<figcaption>
										<div class="cat-description">
											<h3>Women Style</h3>
											<a href="#">Read More</a>
										</div>
									</figcaption>
								</figure>
							</div>
						</div>
					</div>
					<div class="row row-narrow">
						<div class="col-xs-6 col-sm-3">
							<div class="cat-item">
								<figure>
									<a href="shop-full-width.html"><img class="img-responsive" alt="" src="images/flatize/content/products/cat-4.jpg"></a>
									<figcaption>
										<div class="cat-description">
											<h3>Fashion and Style</h3>
											<a href="#">Read More</a>
										</div>
									</figcaption>
								</figure>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="cat-item">
								<figure>
									<a href="shop-full-width.html"><img class="img-responsive" alt="" src="images/flatize/content/products/cat-5.jpg"></a>
									<figcaption>
										<div class="cat-description">
											<h3>Men’s Shoes</h3>
											<a href="#">Read More</a>
										</div>
									</figcaption>
								</figure>
							</div>
						</div>
						<div class="col-xs-6 col-sm-3">
							<div class="cat-item">
								<figure>
									<a href="shop-full-width.html"><img class="img-responsive" alt="" src="images/flatize/content/products/cat-6.jpg"></a>
									<figcaption>
										<div class="cat-description">
											<h3>Watch Style</h3>
											<a href="#">Read More</a>
										</div>
									</figcaption>
								</figure>
							</div>
						</div>
						
					</div>
				</div>
			</section>
			<!-- End About -->
			
			<!-- ------ Begin products  -->
				<!-- Begin Top Selling -->
			<section class="products-slide">
				<div class="container">
					<h2 class="title"><span>Latest Products</span></h2>
					<div class="row">
						<div id="owl-product-slide" class="owl-carousel product-slide">
							<div class="col-md-12">
								
								<div class="item product">
									<div class="product-thumb-info">
										
										<div class="product-thumb-info-image">
											<span class="product-thumb-info-act">
												<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />" class="view-product">
													<span><i class="fa fa-external-link"></i></span>
												</a>
												<a href="<g:createLink controller="shopper" action="cart" params="[adid:'1']" />" class="add-to-cart-product">
													<span><i class="fa fa-shopping-cart"></i></span>
												</a>
											</span>
											<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" class="img-responsive" src="images/flatize/content/products/product-1.jpg"></a>
										</div>
										
										<div class="product-thumb-info-content">
											<span class="price pull-right">29.99 USD</span>
											<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Denim shirt</a></h4>
											<span class="item-cat"><small><a href="#">Jackets</a></small></span>
										</div>
									</div>
								</div>
								
								
							</div>
							<div class="col-md-12">
								<div class="item product">
									<div class="product-thumb-info">
										<div class="product-thumb-info-image">
											<span class="product-thumb-info-act">
												<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />" class="view-product">
													<span><i class="fa fa-external-link"></i></span>
												</a>
												<a href="<g:createLink controller="shopper" action="cart" params="[adid:'1']" />" class="add-to-cart-product">
													<span><i class="fa fa-shopping-cart"></i></span>
												</a>
											</span>
											<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" class="img-responsive" src="images/flatize/content/products/product-2.jpg"></a>
										</div>
										<div class="product-thumb-info-content">
											<span class="price pull-right">25.99 USD</span>
											<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Poplin shirt with fine pleated bands</a></h4>
											<span class="item-cat"><small><a href="#">Shirts</a></small></span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="item product">
									<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">
										<span class="bag bag-hot">Hot</span>
									</a>
									<div class="product-thumb-info">
										<div class="product-thumb-info-image">
											<span class="product-thumb-info-act">
												<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />" class="view-product">
													<span><i class="fa fa-external-link"></i></span>
												</a>
												<a href="<g:createLink controller="shopper" action="cart" params="[adid:'1']" />" class="add-to-cart-product">
													<span><i class="fa fa-shopping-cart"></i></span>
												</a>
											</span>
											<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" class="img-responsive" src="images/flatize/content/products/product-3.jpg"></a>
										</div>
										<div class="product-thumb-info-content">
											<span class="price pull-right">25.99 USD</span>
											<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Contrasting shirt</a></h4>
											<span class="item-cat"><small><a href="#">Stock clearance</a></small></span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="item product">
									<div class="product-thumb-info">
										<div class="product-thumb-info-image">
											<span class="product-thumb-info-act">
												<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />" class="view-product">
													<span><i class="fa fa-external-link"></i></span>
												</a>
												<a href="<g:createLink controller="shopper" action="cart" params="[adid:'1']" />" class="add-to-cart-product">
													<span><i class="fa fa-shopping-cart"></i></span>
												</a>
											</span>
											<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" class="img-responsive" src="images/flatize/content/products/product-4.jpg"></a>
										</div>
										<div class="product-thumb-info-content">
											<span class="price pull-right">59.99 USD</span>
											<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Waistcoat with top stitching</a></h4>
											<span class="item-cat"><small><a href="#">Blazers</a></small></span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="item product">
									<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">
										<span class="bag bag-cool">Cool</span>
									</a>
									<div class="product-thumb-info">
										<div class="product-thumb-info-image">
											<span class="product-thumb-info-act">
												<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />" class="view-product">
													<span><i class="fa fa-external-link"></i></span>
												</a>
												<a href="<g:createLink controller="shopper" action="cart" params="[adid:'1']" />" class="add-to-cart-product">
													<span><i class="fa fa-shopping-cart"></i></span>
												</a>
											</span>
											<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" class="img-responsive" src="images/flatize/content/products/product-5.jpg"></a>
										</div>
										<div class="product-thumb-info-content">
											<span class="price pull-right">39.99 USD</span>
											<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Loose fit ripped jeans</a></h4>
											<span class="item-cat"><small><a href="#">Jeans</a></small></span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- End Top Selling -->
			
	
			<!-- End products -->
			
			
			<!-- Begin List Thumbs -->
			<section class="list-thumbs">
				<div class="container">
					<div class="row">
						<div class="col-sm-4 featured">
							<h3>Featured</h3>
							<div class="row">
								<div id="owl-featured-slide" class="owl-carousel product-slide product-thumbs-slide">
									<div class="col-md-12">
										<div class="item">
											<ul class="list-unstyled list-thumbs-pro">
												<li class="product">
													<div class="product-thumb-info">
														<div class="product-thumb-info-image">
															<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="images/flatize/content/products/product-4.jpg"></a>
														</div>
														
														<div class="product-thumb-info-content">
															<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Waistcoat with top stitching</a></h4>
															<span class="item-cat"><small><a href="#">Jackets</a></small></span>
															<span class="price">29.99 USD</span>
														</div>
													</div>
												</li>
												<li class="product">
													<div class="product-thumb-info">
														<div class="product-thumb-info-image">
															<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="images/flatize/content/products/product-5.jpg"></a>
														</div>
														
														<div class="product-thumb-info-content">
															<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Poplin shirt with fine pleated bands</a></h4>
															<span class="item-cat"><small><a href="#">Jackets</a></small></span>
															<span class="price">29.99 USD</span>
														</div>
													</div>
												</li>
												<li class="product">
													<div class="product-thumb-info">
														<div class="product-thumb-info-image">
															<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="images/flatize/content/products/product-6.jpg"></a>
														</div>
														
														<div class="product-thumb-info-content">
															<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Contrasting shirt</a></h4>
															<span class="item-cat"><small><a href="#">Jackets</a></small></span>
															<span class="price">29.99 USD</span>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
									<div class="col-md-12">
										<div class="item">
											<ul class="list-unstyled list-thumbs-pro">
												<li class="product">
													<div class="product-thumb-info">
														<div class="product-thumb-info-image">
															<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="images/flatize/content/products/product-24.jpg"></a>
														</div>
														
														<div class="product-thumb-info-content">
															<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Chain necklace</a></h4>
															<span class="item-cat"><small><a href="#">Accessories</a></small></span>
															<span class="price">29.99 USD</span>
														</div>
													</div>
												</li>
												<li class="product">
													<div class="product-thumb-info">
														<div class="product-thumb-info-image">
															<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="images/flatize/content/products/product-23.jpg"></a>
														</div>
														
														<div class="product-thumb-info-content">
															<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Double chain necklace</a></h4>
															<span class="item-cat"><small><a href="#">Accessories</a></small></span>
															<span class="price">29.99 USD</span>
														</div>
													</div>
												</li>
												<li class="product">
													<div class="product-thumb-info">
														<div class="product-thumb-info-image">
															<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="images/flatize/content/products/product-22.jpg"></a>
														</div>
														
														<div class="product-thumb-info-content">
															<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Zigzag pattern cap</a></h4>
															<span class="item-cat"><small><a href="#">Caps and Hats</a></small></span>
															<span class="price">29.99 USD</span>
														</div>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-4 bestseller">
							<h3>Bestseller</h3>
							<ul class="list-unstyled list-thumbs-pro">
								<li class="product">
									<div class="product-thumb-info">
										<div class="product-thumb-info-image">
											<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="images/flatize/content/products/product-10.jpg"></a>
										</div>
										
										<div class="product-thumb-info-content">
											<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Striped full skirt</a></h4>
											<span class="item-cat"><small><a href="#">Skirts</a></small></span>
											<span class="price">29.99 USD</span>
										</div>
									</div>
								</li>
								<li class="product">
									<div class="product-thumb-info">
										<div class="product-thumb-info-image">
											<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="images/flatize/content/products/product-11.jpg"></a>
										</div>
										
										<div class="product-thumb-info-content">
											<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Structured double-breasted blazer</a></h4>
											<span class="item-cat"><small><a href="#">Outerwear</a></small></span>
											<span class="price">29.99 USD</span>
										</div>
									</div>
								</li>
								<li class="product">
									<div class="product-thumb-info">
										<div class="product-thumb-info-image">
											<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="images/flatize/content/products/product-12.jpg"></a>
										</div>
										
										<div class="product-thumb-info-content">
											<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Sheer overlay dress</a></h4>
											<span class="item-cat"><small><a href="#">Dresses</a></small></span>
											<span class="price">29.99 USD</span>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class="col-sm-4 trends">
							<h3>Trends</h3>
							<ul class="list-unstyled list-thumbs-pro">
								<li class="product">
									<div class="product-thumb-info">
										<div class="product-thumb-info-image">
											<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="images/flatize/content/products/product-7.jpg"></a>
										</div>
										
										<div class="product-thumb-info-content">
											<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Striped sweater</a></h4>
											<span class="item-cat"><small><a href="#">Stock clearance</a></small></span>
											<span class="price">29.99 USD</span>
										</div>
									</div>
								</li>
								<li class="product">
									<div class="product-thumb-info">
										<div class="product-thumb-info-image">
											<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="images/flatize/content/products/product-8.jpg"></a>
										</div>
										
										<div class="product-thumb-info-content">
											<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Checked shirt with pocket</a></h4>
											<span class="item-cat"><small><a href="#">Shirts</a></small></span>
											<span class="price">29.99 USD</span>
										</div>
									</div>
								</li>
								<li class="product">
									<div class="product-thumb-info">
										<div class="product-thumb-info-image">
											<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="images/flatize/content/products/product-9.jpg"></a>
										</div>
										
										<div class="product-thumb-info-content">
											<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Classic blazer</a></h4>
											<span class="item-cat"><small><a href="#">Outerwear</a></small></span>
											<span class="price">29.99 USD</span>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>				
				</div>
			</section>
			<!-- End List Thumbs -->
			
			<!-- Begin Partners -->
			<section class="partners text-center">
				<div class="container">
					<hr>
					<ul class="list-inline">
						<li>
							<a href="#"><img src="images/flatize/content/partners/partner-1.jpg" alt=""></a>
						</li>
						<li>
							<a href="#"><img src="images/flatize/content/partners/partner-2.jpg" alt=""></a>
						</li>
						<li>
							<a href="#"><img src="images/flatize/content/partners/partner-3.jpg" alt=""></a>
						</li>
						<li>
							<a href="#"><img src="images/flatize/content/partners/partner-4.jpg" alt=""></a>
						</li>
						<li>
							<a href="#"><img src="images/flatize/content/partners/partner-5.jpg" alt=""></a>
						</li>
					</ul>
				</div>
			</section>
			<!-- End Partners -->
		</div>
		<!-- End Main -->
		
	
	

	
</body>
</html>