		<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							
							<i class="fa fa-shopping-cart"></i> <span class="shopping-bag">9</span></a>
							<div class="dropdown-menu">
								<h3>Recently added item(s)</h3>
								<ul class="list-unstyled list-thumbs-pro">
									<li class="product">
										<div class="product-thumb-info">
											<a href="#" class="product-remove"><i class="fa fa-trash-o"></i></a>
											<div class="product-thumb-info-image">
												<a href="shop-product-detail1.html"><img alt="" width="60" src="<g:createLinkTo dir="/"  />images/flatize/content/products/product-1.jpg"></a>
											</div>
											
											<div class="product-thumb-info-content">
												<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Denim shirt</a></h4>
												<span class="item-cat"><small><a href="#">Jackets</a></small></span>
												<span class="price">29.99 USD</span>
											</div>
										</div>
									</li>
									<li class="product">
										<div class="product-thumb-info">
											<a href="#" class="product-remove"><i class="fa fa-trash-o"></i></a>
											<div class="product-thumb-info-image">
												<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="<g:createLinkTo dir="/"  />images/flatize/content/products/product-2.jpg"></a>
											</div>
											
											<div class="product-thumb-info-content">
												<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Poplin shirt with fine pleated bands</a></h4>
												<span class="item-cat"><small><a href="#">Jackets</a></small></span>
												<span class="price">29.99 USD</span>
											</div>
										</div>
									</li>
									<li class="product">
										<div class="product-thumb-info">
											<a href="#" class="product-remove"><i class="fa fa-trash-o"></i></a>
											<div class="product-thumb-info-image">
												<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="<g:createLinkTo dir="/"  />images/flatize/content/products/product-3.jpg"></a>
											</div>
											
											<div class="product-thumb-info-content">
												<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Contrasting shirt</a></h4>
												<span class="item-cat"><small><a href="#">Jackets</a></small></span>
												<span class="price">29.99 USD</span>
											</div>
										</div>
									</li>
								</ul>
								<ul class="list-inline cart-subtotals text-right">
									<li class="cart-subtotal"><strong>Subtotal</strong></li>
									<li class="price"><span class="amount"><strong>$431</strong></span></li>
								</ul>
								<div class="cart-buttons text-right">
									<button class="btn btn-white">View Cart</button>
									<button class="btn btn-primary">Checkout</button>
								</div>
							</div>