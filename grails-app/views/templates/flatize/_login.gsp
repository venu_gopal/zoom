
<div class="login-wrapper">
	
	<ul role="tablist" class="nav nav-tabs pro-tabs">
		<li class="active"><a data-toggle="tab" role="tab" href="#login">Login</a></li>
		<li><a data-toggle="tab" role="tab" href="#register">Register</a></li>

	</ul>
	<div class="tab-content">
		<div id="login" class="tab-pane active">
			<form id="form-login" role="form">
					<div class="form-group">
					<label for="inputusername">Username</label> <input type="text"
						class="form-control input-lg" id="inputusername"
						placeholder="Username">
				</div>
				<div class="form-group">
					<label for="inputpassword">Password</label> <input type="password"
						class="form-control input-lg" id="inputpassword"
						placeholder="Password">
				</div>
				<ul class="list-inline">
					<li><button type="submit" class="btn btn-white">Log in</button></li>
					<li><a href="#">Request new password</a></li>
				</ul>
				
			</form>

 <a class="btn btn-block btn-social btn-twitter">
    <i class="fa fa-twitter"></i> Sign in with Twitter
  </a>

 <a class="btn btn-block btn-social btn-facebook">
    <i class="fa fa-facebook"></i> Sign in with Facebook
  </a>
  
   <a class="btn btn-block btn-social btn-google-plus">
    <i class="fa fa-google-plus"></i> Sign in with Google
  </a>
  
		</div>
		<div id="register" class="tab-pane">
			
			<form id="form-register" role="form">
				<h4>Register</h4>
						<div class="form-group">
					<label for="inputname">Name</label> <input type="text"
						class="form-control input-lg" id="inputname"
						placeholder="Your Name">
				</div>
				
					<div class="form-group">
					<label for="inputusername">Email</label> <input type="text"
						class="form-control input-lg" id="inputusername"
						placeholder="Your email">
				</div>
				<div class="form-group">
					<label for="inputpassword">Password</label> <input type="password"
						class="form-control input-lg" id="inputpassword"
						placeholder="Password">
				</div>
				
				<button type="submit" class="btn btn-white">Register</button>
			</form>
		</div>

	</div>


</div>

