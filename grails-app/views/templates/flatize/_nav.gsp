<nav class="navbar navbar-default navbar-main navbar-main-narrow" role="navigation">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
						<a class="logo" href="<g:createLinkTo dir="/"  />"><img src="<g:createLinkTo dir="/"  />images/newlogo.png" alt="Haggell"></a> </div>
					<ul class="nav nav-pills nav-top navbar-right navbar-act">
						<li class="login"><a href="javascript:void(0);"><i class="fa fa-user"></i></a></li>
						<li class="search"><a href="javascript:void(0);" data-toggle="modal" data-target=".bs-example-modal-lg"><i class="fa fa-search"></i></a></li>
						
						<li class="dropdown menu-shop">
						
					<g:render template="/templates/flatize/cart" />
							
							
						</li>
						
					</ul>
					<div class="navbar-collapse collapse width">
						<ul class="nav navbar-nav nav-white">
							<li class="dropdown active"> <a href="<g:createLinkTo dir="/"  />" >Home</a>
							</li>
							
							<li class="dropdown megamenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Shop</a>
								<div class="dropdown-menu">
									<div class="mega-menu-content">
										<div class="row">
											<div class="col-md-4 hidden-sm hidden-xs menu-column">
												<h3>Trends</h3>
												<ul class="list-unstyled sub-menu list-thumbs-pro">
													<li class="product">
														<div class="product-thumb-info">
															<div class="product-thumb-info-image">
																<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="<g:createLinkTo dir="/"  />images/flatize/content/products/product-1.jpg"></a>
															</div>
															
															<div class="product-thumb-info-content">
																<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Denim shirt</a></h4>
																<span class="item-cat"><small><a href="#">Jackets</a></small></span>
																<span class="price">29.99 USD</span>
															</div>
														</div>
													</li>
													<li class="product">
														<div class="product-thumb-info">
															<div class="product-thumb-info-image">
																<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="<g:createLinkTo dir="/"  />images/flatize/content/products/product-2.jpg"></a>
															</div>
															
															<div class="product-thumb-info-content">
																<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Poplin shirt with fine pleated bands</a></h4>
																<span class="item-cat"><small><a href="#">Jackets</a></small></span>
																<span class="price">29.99 USD</span>
															</div>
														</div>
													</li>
													<li class="product">
														<div class="product-thumb-info">
															<div class="product-thumb-info-image">
																<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img alt="" width="60" src="<g:createLinkTo dir="/"  />images/flatize/content/products/product-3.jpg"></a>
															</div>
															
															<div class="product-thumb-info-content">
																<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Contrasting shirt</a></h4>
																<span class="item-cat"><small><a href="#">Jackets</a></small></span>
																<span class="price">29.99 USD</span>
															</div>
														</div>
													</li>
												</ul>
											</div>
											<div class="col-md-2 menu-column">
												<h3>Man</h3>
												<ul class="list-unstyled sub-menu">
													<li><a href="#">Jackets</a></li>
													<li><a href="#">Blazers</a></li>
													<li><a href="#">Suits</a></li>
													<li><a href="#">Trousers</a></li>
													<li><a href="#">Jeans</a></li>
													<li><a href="#">Shirts</a></li>
													<li><a href="#">Sweatshirts &amp; Hoodies</a></li>
													<li><a href="#">Swimwear</a></li>
													<li><a href="#">Accessories</a></li>
												</ul>
											</div>
											
											<div class="col-md-2 menu-column">
												<h3>Woman</h3>
												<ul class="list-unstyled sub-menu">
													<li><a href="#">Coats</a></li>
													<li><a href="#">Outerwear</a></li>
													<li><a href="#">Dresses</a></li>
													<li><a href="#">Tops</a></li>
													<li><a href="#">Trousers</a></li>
													<li><a href="#">Shirts</a></li>
													<li><a href="#">Jeans</a></li>
													<li><a href="#">T-shirts</a></li>
													<li><a href="#">Shoes</a></li>
													<li><a href="#">Handbags</a></li>
													<li><a href="#">Stock clearance</a></li>
												</ul>
											</div>
											<div class="col-md-4 hidden-sm hidden-xs menu-column">
												<h3>Explore new collection</h3>
												<ul class="list-unstyled sub-menu list-md-pro">
													<li class="product">
														<div class="product-thumb-info">
															<div class="product-thumb-info-image">
																<a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />"><img class="img-responsive" width="330" alt="" src="<g:createLinkTo dir="/"  />images/flatize/content/products/ad-1.png"></a>
															</div>
															
															<div class="product-thumb-info-content">
																<h4><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Men’s Fashion and Style</a></h4>
																<p>Whatever you’re looking for, be it the latest fashion trends, cool outfit ideas or new ways to wear your favourite pieces, we’ve got all the style inspiration you need.</p>
															</div>
														</div>
													</li>
												</ul>
											</div>
										</div>
									</div>
								</div>
							</li>
							
									<!-- 	
							
							<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages</a>
								<ul class="dropdown-menu">
									<li class="dropdown-submenu">
										<a href="#">Shop</a>
										<ul class="dropdown-menu">
											<li><a href="shop-full-width.html">Shop - Full Width</a></li>
											<li><a href="shop-sidebar.html">Shop - Sidebar</a></li>
											<li><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Shop - Product Detail 1</a></li>
											<li><a href="<g:createLink controller="deal" action="show" params="[adid:'1']" />">Shop - Product Detail 2</a></li>
											<li><a href="shop-cart-full.html">Shop - Cart Full</a></li>
											<li><a href="shop-cart-sidebar.html">Shop - Cart Sidebar</a></li>
											<li><a href="shop-checkout.html">Shop - Checkout</a></li>
										</ul>
									</li>
									<li><a href="index-white.html">Mega Menu White</a></li>
									<li><a href="page-404.html">404 Error Page</a></li>									
								</ul>
							</li>
							
							
							
				
							<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog</a>
								<ul class="dropdown-menu">
									<li><a href="blog-sidebar.html">Blog Sidebar</a></li>
									<li><a href="blog-mini-sidebar.html">Blog Mini Sidebar</a></li>
									<li><a href="blog-masonry.html">Blog Masonry</a></li>
									<li><a href="blog-single.html">Blog Single</a></li>
								</ul>
							</li> -->
							
							<li><a href="<g:createLinkTo dir="/"  />shopper/profile">My Profile</a></li>
							<li><a href="<g:createLinkTo dir="/"  />contactus" >Contact</a>
								
							</li>
							
						</ul>
					</div><!--/.nav-collapse --> 
				</div><!--/.container-fluid --> 
			</nav>
		
		