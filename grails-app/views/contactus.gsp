<!DOCTYPE html>
<html lang="en">
<head>
<meta name="layout" content="anonymous" />
</head>
<body>
	<div id="page">
	

		<!-- Begin Main -->
		<div role="main" class="main">
		
			<!-- Begin page top -->
			<section class="page-top-lg">
				<div class="container">
					<div class="page-top-in">
						<h2><span>Get In Touch</span></h2>
					</div>
				</div>
			</section>
			<!-- End page top -->
			<div class="container">
				<div class="contact-content contact-content-full">
					<div class="contact-intro">
						<h3>ALOHA!</h3>
						<p>
						Get In Touch with us for any suggestions, complaints or thanks
						</p>
					</div>
					<div class="row">
						<div class="col-md-4">
							<h4><i class="fa fa-map-marker"></i> United states</h4>
							<address>5th Avenue, 10019 New York, United States<br>+1 310 666 8888<br>+1 310 666 9999<br><a href="mailto:ushaggel@alphasigma.com.au">ushaggel@alphasigma.com.au</a></address>
						</div>
						<div class="col-md-4">
							<h4><i class="fa fa-map-marker"></i> Canada</h4>
							<address>Alexander Street, Vancouver, BC V6A 1E1, Canada<br>+1 310 666 8888<br>+1 310 666 9999<br><a href="mailto:canhaggel@alphasigma.com.au">canhaggel@alphasigma.com.au</a></address>
						</div>
						<div class="col-md-4">
							<h4><i class="fa fa-map-marker"></i> Australia</h4>
							<address> 1 Point Park Crescent Docklands Melbourne 3008 <br>+1300 HAGGELL<br><br><a href="mailto:aushaggel@alphasigma.com.au">aushaggel@alphasigma.com.au</a></address>
						</div>
					</div>
					<hr class="tall">
					<h3>Contact Form</h3>
					<div class="row">
						<div class="col-xs-5 col-sm-4">
							<div class="form-group">
								<label for="yname">Your Name*</label>
								<input type="text" class="form-control" id="yname" name="yname">
							</div>
							<div class="form-group">
								<label for="yemail">Your Email*</label>
								<input type="email" class="form-control" id="yemail" name="yemail">
							</div>
							<div class="form-group">
								<label for="subject">Subject</label>
								<input type="text" class="form-control" id="subject" name="subject">
							</div>
						</div>
						
						<div class="col-xs-7 col-sm-8">
							<div class="form-group">
								<label for="ymessage">Your Message*</label>
								<textarea class="form-control" id="ymessage" name="ymessage" rows="8"></textarea>
							</div>
						
							<div class="form-group">
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</div>
					
					
				</div>
			</div>
		</div>
		<!-- End Main -->
		
		
			
	</div>

	
	
</body>
</html>