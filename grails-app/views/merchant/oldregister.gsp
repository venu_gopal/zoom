<!DOCTYPE html>
<html>
	<head>
	<meta name="layout" content="main"/>
	
		<!-- Title here -->
		<title>Merchant Register - Haggell</title>
		

	</head>                
	
	<body>

	 <g:render template="/templates/cart" />
           
      <!-- Logo & Navigation starts -->
      
	<g:render template="/templates/firstBar" />
      
      <!-- Logo & Navigation ends -->
      	<!-- Second Bar - categories starts -->
	<g:render template="/templates/secondBar" />
	<!-- Second Bar - categories ends -->

  <g:render template="/templates/login" />
 <g:render template="/templates/register" />
     
      <!-- Page title -->
      <div class="page-title">
         <div class="container">
            <h2><i class="fa fa-user color"></i> Merchant Register</h2>
            <hr />
         </div>
      </div>
      <!-- Page title -->
      
      <!-- Page content -->
		  
		  <div class="shop-items">
			<div class="container">
			   <div class="row">
				  <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
				   
                 <!-- Description, specs and review -->
                 <ul id="myTab" class="nav nav-tabs">
                  <!-- Use uniqe name for "href" in below anchor tags -->
                  <li class="active"><a href="#tab1" data-toggle="tab">Profile</a></li>
                  <li><a href="#tab2" data-toggle="tab">Contact</a></li>
                  <li><a href="#tab3" data-toggle="tab">Location</a></li>
                 </ul>

                    <!-- Tab Content -->
					<div id="myTabContent" class="tab-content">
                      <!-- Description -->
                      <div class="tab-pane fade in active" id="tab1">
                        <div class="col-md-offset-2 col-sm-7 col-md-7 col-lg-7 col-xs-12 form-sty">
                      <!-- Description -->
                      <div class="tab-pane fade in active" id="tab1">
                       <form class="form-horizontal" role="form">
						  <div class="form-group form-group-lg">
							<label class="col-sm-4 control-label text-align" for="formGroupInputLarge">Email</label>
							<div class="col-sm-8">
							  <input class="form-control" type="email" id="formGroupInputLarge" placeholder="Email">
							</div>
						  </div>
						  <div class="form-group form-group-sm">
							<label class="col-sm-4 control-label text-align" for="formGroupInputSmall">Password</label>
							<div class="col-sm-8">
							  <input class="form-control" type="password" id="formGroupInputSmall" placeholder="Password">
							</div>
						  </div>
						  <div class="form-group form-group-sm">
							<label class="col-sm-4 control-label text-align" for="formGroupInputSmall">Describe your business</label>
							<div class="col-sm-8">
							  <textarea class="form-control" rows="7"></textarea>
							</div>
						   </div>
						  <a class="btn btn-info pull-right"  data-toggle="tab"  href="#tab2"  onClick="tab(1)">Next 
						 <i class="fa fa-angle-double-right"></i></a>
						</form>
                       </div>
					 </div>
                    </div>
					 
                     <!-- tab2 -->
                     <div class="tab-pane fade" id="tab2">
					  <div class="col-md-offset-2 col-sm-7 col-md-7 col-lg-7 col-xs-12 form-sty">
                        <form class="form-horizontal" role="form">
						  <div class="form-group form-group-lg">
						   <label class="col-sm-4 control-label text-align" for="formGroupInputLarge">Phone</label>
							<div class="col-sm-8">
							  <input class="form-control" type="text" id="formGroupInputLarge" placeholder="Phone">
							</div>
						   </div>
						   <div class="form-group form-group-sm">
							<label class="col-sm-4 control-label text-align" for="formGroupInputSmall">Website</label>
							<div class="col-sm-8">
							  <input class="form-control" type="text" id="formGroupInputSmall" placeholder="Website">
							</div>
						   </div>
						  <div class="form-group form-group-sm">
							<label class="col-sm-4 control-label text-align" for="formGroupInputSmall">Logo</label>
							<div class="col-sm-8">
							<!-- Dropdown menu -->
							<input type="file" title="Search for a file to add" class="btn-primary">
						   </div>
						  </div>
							<a class="btn btn-info pull-right"  data-toggle="tab"  href="#tab3"  onClick="tab(2)">Next 
							<i class="fa fa-angle-double-right"></i></a>
						</form>
					   </div>
                      </div>
					  
                      <!-- tab3 -->
                      <div class="tab-pane fade" id="tab3">
					  <div class="col-md-offset-3 col-sm-6 col-md-6 col-lg-6 col-xs-12 form-sty">
                       <form class="form-horizontal" role="form">
						<div class="form-group form-group-lg">
						 <label class="col-sm-4 control-label text-align" for="formGroupInputLarge">Address</label>
						  <div class="col-sm-8">
							<input class="form-control" type="email" id="formGroupInputLarge" placeholder="Email">
							</div>
						    <img src="img/staticmap.png" class="map-sty" alt=""/>
						  </div>
						 <button type="submit" class="btn btn-info pull-right"> <i class="fa fa-check"></i> Finish</button>
						</form> 
					   </div>
                      </div>
                    </div>
                  </div>
               
				<!-- <div class="sep"></div>  --> 
				 </div>
				</div>  <!-- Recent posts Starts -->
			 
			  <!-- Recent posts Starts -->
					
			  <!-- Recent posts Ends -->		
			  
			  
			  <!-- Catchy starts -->
				   
			  <!-- Catchy ends -->
			  
			  <!-- CTA Starts -->
			  
			  <!-- CTA Ends -->
      
      <!-- Footer starts -->
      <footer>
       <div class="container">
         <div class="row">
			<div class="col-md-4 col-sm-4">
              <div class="fwidget">
                <h4>About Us</h4> 
                  <hr />
                  <p>Duis leo risus, vehicula luctus nunc.  Quiue rhoncus, a sodales enim arcu quis turpis. Duis leo risus, condimentum ut posuere ac, vehicula luctus nunc.  Quisque rhoncus, a sodales enim arcu quis turpis.</p>
                  <div class="social">
                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    <a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-4">
               <div class="fwidget">
                <h4>Categories</h4>
                 <hr />
                    <ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Start Selling</a></li>
						<li><a href="login.html">Signin</a></li>
						<li><a href="register.html">Join</a></li>
						<li><a href="account.html">My profile</a></li>
						<li><a href="#">My Sales</a></li>
						<li><a href="orderhistory.html">My Purchases</a></li>
						<li><a href="contactus.html">Contact us</a></li>
					</ul>
                   </div>
                  </div> 
				  
				  <div class="col-md-4 col-sm-4">
                      <div class="fwidget">
                       <h4>Get In Touch</h4>
                           <hr />
                           <div class="address">
                              <p><i class="fa fa-home color contact-icon"></i> #12, Plot No.14, Raj Karmara Street, </p>
                              <p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 5th Stage, Koramangala, Madiwala,</p>
                              <p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Bangalore - 493922, Karananana.</p>
                              <p><i class="fa fa-phone color contact-icon"></i> +94-948-323-5532</p>
                              <p><i class="fa fa-envelope color contact-icon"></i> <a href="mailto:something@gmail.com">info@olsonkart.com</a></p>
                           </div>
                         </div>
                       </div>
                     </div>
					<hr />
                <div class="copy text-center">
               Copyright 2014 &copy; - Haggell
            </div>
         </div>
      </footer>
      <!-- Footer ends -->
     
  <!-- JavaScript Includes -->
		
		
		
      <!-- Scroll to top -->
      <span class="totop"><a href="#"><i class="fa fa-chevron-up"></i></a></span> 
      <script src="http://gregpike.net/demos/bootstrap-file-input/bootstrap.file-input.js.pagespeed.jm.IdmWcpRIii.js"></script>
<script>$(document).ready(function(){$('input[type=file]').bootstrapFileInput();});</script>
<script>//<![CDATA[
(function(){var d=encodeURIComponent,f=window,g=document,h="documentElement",k="length",l="prototype",m="body",p="&",s="&ci=",t=",",u="?",v="Content-Type",w="Microsoft.XMLHTTP",x="Msxml2.XMLHTTP",y="POST",z="application/x-www-form-urlencoded",A="img",B="input",C="load",D="oh=",E="on",F="pagespeed_url_hash",G="url=";f.pagespeed=f.pagespeed||{};var H=f.pagespeed,I=function(a,b,c){this.c=a;this.e=b;this.d=c;this.b=this.f();this.a={}};I[l].f=function(){return{height:f.innerHeight||g[h].clientHeight||g[m].clientHeight,width:f.innerWidth||g[h].clientWidth||g[m].clientWidth}};I[l].g=function(a){a=a.getBoundingClientRect();return{top:a.top+(void 0!==f.pageYOffset?f.pageYOffset:(g[h]||g[m].parentNode||g[m]).scrollTop),left:a.left+(void 0!==f.pageXOffset?f.pageXOffset:(g[h]||g[m].parentNode||g[m]).scrollLeft)}};I[l].h=function(a){if(0>=a.offsetWidth&&0>=a.offsetHeight)return!1;a=this.g(a);var b=a.top.toString()+t+a.left.toString();if(this.a.hasOwnProperty(b))return!1;this.a[b]=!0;return a.top<=this.b.height&&a.left<=this.b.width};I[l].i=function(a){var b;if(f.XMLHttpRequest)b=new XMLHttpRequest;else if(f.ActiveXObject)try{b=new ActiveXObject(x)}catch(c){try{b=new ActiveXObject(w)}catch(e){}}if(!b)return!1;b.open(y,this.c+(-1==this.c.indexOf(u)?u:p)+G+d(this.e));b.setRequestHeader(v,z);b.send(a);return!0};I[l].k=function(){for(var a=[A,B],b=[],c={},e=0;e<a[k];++e)for(var q=g.getElementsByTagName(a[e]),n=0;n<q[k];++n){var r=q[n].getAttribute(F);r&&(q[n].getBoundingClientRect&&this.h(q[n]))&&!(r in c)&&(b.push(r),c[r]=!0)}if(0!=b[k]){a=D+this.d;a+=s+d(b[0]);for(e=1;e<b[k];++e){c=t+d(b[e]);if(131072<a[k]+c[k])break;a+=c}H.criticalImagesBeaconData=a;this.i(a)}};H.j=function(a,b,c){if(a.addEventListener)a.addEventListener(b,c,!1);else if(a.attachEvent)a.attachEvent(E+b,c);else{var e=a[E+b];a[E+b]=function(){c.call(this);e&&e.call(this)}}};H.l=function(a,b,c){var e=new I(a,b,c);H.j(f,C,function(){f.setTimeout(function(){e.k()},0)})};H.criticalImagesBeaconInit=H.l;})();pagespeed.criticalImagesBeaconInit('/mod_pagespeed_beacon','http://gregpike.net/demos/bootstrap-file-input/demo.html','TFEmFH3ngh');
//]]></script>
	

<r:require modules="core"/>
  

	 <script>
	function tab(id)
	{
	$("#myTab li").removeClass('active');
	$("#myTab li:eq("+id+")").addClass('active');
	}
     </script>	 
	  
	
	  
	</body>	
</html>