<html>
<head>

<title><g:layoutTitle default="Haggell" /></title>
<link rel="shortcut icon" href="#">

<meta charset="utf-8">
<meta name="description" content="Your description">
<meta name="keywords" content="Your,Keywords">
<meta name="author" content="alphasigma for haggell">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="layout" content="application">

<g:layoutHead />

<!--  STYLE SHEETS -->
<r:require module="stylesheets" />
<r:layoutResources />



</head>
<body>
	<sec:ifAnyGranted roles="ROLE_USER">
		<g:render template="/templates/cart" />
	</sec:ifAnyGranted>
	<!-- Logo & Navigation starts -->
	<g:render template="/templates/firstBar" />
	<!-- Logo & Navigation ends -->
	<!-- Second Bar - categories starts -->
	<g:render template="/templates/secondBar" />
	<!-- Second Bar - categories ends -->


	<g:layoutBody />




	<!-- Footer starts -->
	<footer>
		<div class="container">

			<div class="row">

				<div class="col-md-4 col-sm-4">
					<div class="fwidget">

						<h4>About Us</h4>
						<hr />
						<p>Get personalized deals straight from the merchants</p>

						<div class="social">
							<a href="#" class="facebook"><i class="fa fa-facebook"></i></a> <a
								href="#" class="twitter"><i class="fa fa-twitter"></i></a> <a
								href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
							<a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a> <a
								href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
						</div>
					</div>
				</div>

				<div class="col-md-4 col-sm-4">
					<div class="fwidget">
						<h4>Categories</h4>
						<hr />
						<ul>
							<li><a href="#">Home</a></li>
							<li><a href="#">Start Selling</a></li>
							<li><a href="login.html">Signin</a></li>
							<li><a href="register.html">Join</a></li>
							<li><a href="account.html">My profile</a></li>
							<li><a href="#">My Sales</a></li>
							<li><a href="orderhistory.html">My Purchases</a></li>
							<li><a href="contactus.html">Contact us</a></li>
						</ul>
					</div>
				</div>



				<div class="col-md-4 col-sm-4">
					<div class="fwidget">

						<h4>Get In Touch</h4>
						<hr />
						<div class="address">
							<p>
								<i class="fa fa-home color contact-icon"></i> 1 Point Park
								Crescent
							</p>
							<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Docklands</p>
							<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Melbourne Australia</p>
							<p>
								<i class="fa fa-phone color contact-icon"></i> 1300-HAGGELL
							</p>
							<p>
								<i class="fa fa-envelope color contact-icon"></i> <a
									href="mailto:something@gmail.com">haggell@alphasigma.com.au</a>
							</p>
						</div>

					</div>
				</div>

			</div>



			<hr />

			<div class="copy text-center">Copyright 2014 &copy; - Haggell</div>
		</div>
	</footer>

<r:require module="homejs" />
	<r:layoutResources />

	<!-- Javascript files -->
	<!-- jQuery -->



	
</body>
</html>