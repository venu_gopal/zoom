﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>

    <meta name="layout" content="application">

    <style>
    .centerer {
        margin: 0 auto !important;
        float: none !important;
    }
    </style>

    <title>Zooom Deals</title>

</head>

<body class="metrouicss">

<div class="page secondary">

    <g:render template="/templates/header"/>


    <div class="page-region">

        <div class="page-region-content">
            <div class="grid">
                <div class="row">
                    <div class="span7 centerer">
                        <div id="register">
                            <form action="doregister" autocomplete="on" name="shopperForm" id="shopperForm">
                                <h1>Register</h1>


                                <div class="input-control text">
                                    <label for="name" class="uname" data-icon="m">Name</label> <input
                                        class="zoomTextbox" id="name" name="name" required="required"
                                        type="text"/>

                                </div>


                                <div class="input-control text">
                                    <label for="emailsignup" class="youmail" data-icon="m">Email</label> <input
                                        id="emailsignup" name="email" required="required" type="email"/>

                                </div>


                                <div class="input-control password">
                                    <label for="passwordsignup" class="youpasswd"
                                           data-icon="n">Your password</label> <input id="passwordsignup"
                                                                                      name="password"
                                                                                      required="required"
                                                                                      type="password"/>
                                    <button class="btn-reveal"></button>
                                </div>


                                <input type="submit" value="Register"/>

                                <p class="change_link">
                                    Already a member ? <a href="#tologin" id="tologin" class="tologin">Go and log in</a>
                                </p>
                            </form>
                            <br> <br>

                            <div class="progress-bar">
                                <div class="bar bg-color-pink" style="width: 30%"></div>

                                <div class="bar bg-color-yellow" style="width: 30%"></div>

                                <div class="bar bg-color-green" style="width: 40%"></div>
                            </div>
                            <br> <br> <br> <br>

                            <div class="span5" style="float:right">
                                Business Users. <a href="../business/register">Click here</a></div>

                        </div>

                        <div id="login" style="display: none;">
                            <g:render template="/templates/login"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

</div>


<div class="notices" id="status" style="display: none">
    <div class="bg-color-green">

        <div class="notice-header fg-color-white">Registered!</div>
        <!-- TODO remove this and login user automatically -->
        <div class="notice-text">
            You are now registered. Please <a href="#tologin" class="tologin">login to continue</a>..
        </div>
    </div>
</div>


<div class="notices" id="successlogin" style="display: none">
    <div class="bg-color-green">

        <div class="notice-header fg-color-white">Access granted!</div>

        <div class="notice-text">You are now logged in! Redirecting you to home page...</div>
    </div>
</div>


<script src="<g:resource dir="js" file="/metroui/dialog.js" absolute="true"/>"></script>
<script src="<g:resource dir="js" file="/jquery/jquery.form.js" absolute="true"/>"></script>


<script type="text/javascript">
    $(document).ready(function () {

        $('.tologin').click(function () {
            $("#register").hide("slow");
            $('#login').show("slow");

        });

        $('#toregister').click(function () {
            $("#register").show("slow");
            $('#login').hide("slow");

        });

        var options = {
            success: showResponse,
            dataType: 'json'
            //timeout:   3000
        };


        $('#shopperForm').submit(function () {
            $(this).ajaxSubmit(options);
            return false;
        });

        function showResponse(responseText, statusText, xhr, $form) {
            if (responseText.errorDescription) {
                showMsg("Registration error", responseText.errorDescription)
            } else {
                $("#shopperForm").hide();
                $("#status").fadeTo(500, 100, function () {
                    //$(this).html("You are now registered!");
                })
            }
        }

    });
</script>
</body>
</html>