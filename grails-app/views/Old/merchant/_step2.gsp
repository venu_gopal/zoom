<div id="step-2" class="tab-pane fade in active br-lblue">

    <div class="form-group">
        <div class="col-sm-12">
            <label for="phone" class="youmail" data-icon="m">Phone</label>
            <input id="phone" name="phone" class="form-control" required="required" type="text"/>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <label for="website" class="youmail" data-icon="m">Website</label>
            <input id="website" class="form-control" name="website" required="required" type="text"/>
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-12">
            <label for="logo" class="youmail" data-icon="m">Your Logo</label>
            <input id="logo" name="logo" class="form-control" required="required" type="file"/>
        </div>
    </div>
</div>