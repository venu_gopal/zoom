<!DOCTYPE html>
<html>
<head>
    <!-- Title here -->
    <title>Zoomdeals, Login/Register</title>
    <!-- Description, Keywords and Author -->
    <meta name="layout" content="public"/>
</head>

<body>
<div class="wrapper white">
    <div class="inner-page">
        <div class="container">
            <div class="page-mainbar login">
                <!-- Login/Register Content -->
                <div class="login-content">
                    <!-- Nab Bar Tan Menu list -->
                    <ul id="mytab" class="nav nav-tabs nav-jsignUpAjaxustify">
                        <li class="active">
                            <a href="#login" id="loginDivButton" class="br-red" data-toggle="tab">
                                <!-- Icon -->
                                <i class="fa fa-sign-in"></i>
                                <span>Login</span>
                            </a>
                        </li>

                        <li>
                    </ul>

                    <div class="tab-content">
                        <g:render template="/templates/login"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>