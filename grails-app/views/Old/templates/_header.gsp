<div class="page-header">
    <div class="page-header-content">

        <ul id="menu">
            <li><g:link
                    url="${resource(dir: '/')}"><img src="${resource(dir: '/images', file: 'home.png')}"></g:link></li>

            <sec:ifNotLoggedIn>

                <li><g:link
                        url="${resource(dir: '/shopper', file: 'register')}">Register/Login</g:link></li>

            </sec:ifNotLoggedIn>
            <sec:ifLoggedIn>
                <sec:ifAnyGranted roles="ROLE_USER">
                    <li><g:link
                            url="${resource(dir: '/shopper', file: 'purchases')}">My Purchases</g:link></li>
                </sec:ifAnyGranted>
                <sec:ifAnyGranted roles="ROLE_MERCHANT">
                    <li><g:link
                            url="${resource(dir: '/merchant', file: 'createDeal')}">Create Deal</g:link>
                    </li>
                    <li><g:link
                            url="${resource(dir: '/merchant', file: 'sales')}">My Sales</g:link>
                    </li>
                </sec:ifAnyGranted>

                <sec:ifAnyGranted roles="ROLE_ADMIN">

                </sec:ifAnyGranted>
            </sec:ifLoggedIn>

            <li><g:link
                    url="${resource(dir: '/', file: 'contactus')}">Contact Us</g:link>
            </li>

            <sec:ifLoggedIn>
                <li>
                    <g:link
                            url="${resource(dir: '/', file: 'logout')}">Logout</g:link></li>
            </sec:ifLoggedIn>
            <li>
                <span id="shopcart" style="align:right">
                    <span id="simpleCart_grandTotal" class="simpleCart_grandTotal"></span>
                    <a href="javascript:;" class="simpleCart_checkout">Checkout</a>
                </span>
            </li>
        </ul>

    </div>
</div>