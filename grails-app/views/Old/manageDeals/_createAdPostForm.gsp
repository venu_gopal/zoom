<div class="col-md-12">
    <div class="col-md-4">
    </div>

    <div class="col-md-4">
    </div>

    <div class="col-md-4">
        <a href="javascript:void(0)" class="btn btn-success btn-lg"
           id="addDealsLink" %{--onclick="displayCreateAdDealForm()"--}%><i class="fa fa-plus"></i> &nbsp;Add New Deal
        </a>

    </div>
</div>

<div class="col-md-12" id="postDeal">

    <div id="registerdPost" style="display: none">
        <form action="${createLink(controller: 'deal', action: 'saveDeal')}" autocomplete="on"
              class="form-horizontal form-group" name="dealForm" id="dealForm">
            <input type="hidden" name="lat" value="0.0"> <input type="hidden" name="lon" value="0.0">

            <h1>Add Deal</h1>

            <div id="wizard" class="swMain tab-content">
                <ul>
                    <li><a href="#step-1"><label class="stepNumber">1</label> <span
                            class="stepDesc">Describe your Deal<br/>

                    </span>
                    </a></li>
                    <li><a href="#step-2"><label class="stepNumber">2</label> <span
                            class="stepDesc">Price Your Deal<br/>
                    </span>
                    </a></li>

                    <li><a href="#step-3"><label class="stepNumber">3</label> <span
                            class="stepDesc">Locate Your Deal<br/>
                    </span>
                    </a></li>
                    <li><a href="#step-4"><label class="stepNumber">4</label> <span
                            class="stepDesc">Terms & Conditions<br/>
                    </span>
                    </a></li>
                </ul>

                <g:render template="/manageDeals/createDealStep1"/>
                <g:render template="/manageDeals/createDealStep2"/>
                <g:render template="/manageDeals/createDealStep3"/>
                <g:render template="/manageDeals/createDealStep4"/>
            </div>



            <br> <br>

            <div class="progress-bar">
                <div class="bar bg-color-pink" style="width: 30%"></div>

                <div class="bar bg-color-yellow" style="width: 30%"></div>

                <div class="bar bg-color-green" style="width: 40%"></div>
            </div>
            <br> <br> <br> <br>

        </form>
    </div>

</div>
