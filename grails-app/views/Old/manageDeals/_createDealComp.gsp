<div class="page-mainbar login" style="padding-left: 0">
    <!-- Login/Register Content -->
    %{--<div class="">--}%
        %{--<ul id="merchantCreateDealsListDiv" style="position: relative;padding-left: 0;">--}%

        %{--</ul>--}%
    %{--</div>--}%


    <div class="login-content"
         style="max-width: 700px;padding-top: 0px;padding-right: 0;margin-top: 0px">
        <g:render template="/manageDeals/createAdPostForm"/>
    </div>
</div>


<div id="singleCreateDealDiv" class="container-fluid" style="display:none">
    <g:render template="/manageDeals/singleDealCard"/>
</div>

%{--</div>--}%




<script>
    $(document).ready(function () {

        initializeMaps();
        $("#backBtn").hide();

        var options = {
            success: showResponse,
            dataType: 'json'
            //timeout:   3000
        };

        $('#wizard').smartWizard({onLeaveStep: leaveAStepCallback, onFinish: onFinishCallback, updateHeight: true, onShowStep: mapChecker});

        $('#endDate').datetimepicker();

        $( "#endDate" ).datepicker();


        //$('#dealForm').submit(function() {
        //$(this).ajaxSubmit(options);
        //return false;
        //});


        function leaveAStepCallback(obj) {
            var step_num = obj.attr('rel'); // get the current step number
            return validateSteps(step_num); // return false to stay on step and true to continue navigation
        }

        function mapChecker(obj) {
            var step_num = obj.attr('rel'); // get the current step number
            if (step_num == 3 && google) {
                google.maps.event.trigger(window.map, 'resize');
                showCurLoc();
            }
        }

        function onFinishCallback() {
            if (validateAllSteps()) {
                $("#dealForm").ajaxSubmit(options);
                //$("#merchantForm").ajaxSubmit(options);
                return false;
            }
//            $("#register").hide();
////            if (responseText.errorDescription) {
////                showMsg("Registration error",
////                        responseText.errorDescription)
////            } else {
//                $("#dealForm").hide("slow");
//                alert('Deal Saved')
//                $("#addDealStatus").fadeTo(500, 100, function() {
//                    //$(this).html("You are now registered!");
//                })
//
////            }

        }

        // Your Step validation logic
        function validateSteps(stepnumber) {
            var isStepValid = true;
            // validate step 1
            if (stepnumber == 1) {
                validateStep1();
                // Your step validation logic
                // set isStepValid = false if has errors
                //alert("validating step one")
            } else if (stepnumber == 2) {
                //alert("validating step two")
            }
            return isStepValid;
        }

        function validateAllSteps() {
            var isStepValid = true;
            // all step validation logic
//            alert("validating all setps")
            //tinyMCE.triggerSave();
            return isStepValid;
        }

        function showResponse(responseText, statusText, xhr, $form) {
            $("#registerdPost").hide();
            //$("#merchantForm").hide();
            if (responseText.errorDescription) {
                showMsg("Registration error",
                        responseText.errorDescription)
            } else {

                $("#dealForm").hide("slow");
                $("#merchantForm").hide("slow");
                $("#addDealStatus").fadeTo(500, 100, function () {
                    //$(this).html("You are now registered!");
                })
                $("#merchanrStatus").fadeTo(500, 100, function () {
                    //$(this).html("You are now registered!");
                })
                getNewDealJSON("${createLink(controller: 'deal',action: 'getRecentMerchantDealJSON')}");


            }
        }

        $("#step-1").validate({
            rules: {
                title: {
                    required: true

                },
                description: {
                    required: true

                }
            },
            messages: {
                title: {
                    required: ""
                }
            }
        });

    });
    function validateStep1() {
        $("#dealForm").validate({

            rules: {
                title: {
                    required: true

                },
                description: {
                    required: true

                }
            },
            messages: {
                title: {
                    required: ""
                }
            }
        });
    }

    function getMerchantDealsJSON(formPostUrl) {

        var template = $('#singleCreateDealDiv').html();
        $.getJSON(formPostUrl, function (data) {
            $.each(data, function (idx, obj) {
                //alert(obj.imageURL);
                var output = Mustache.render(template, obj);
                $('#merchantCreateDealsListDiv').prepend(output);
            });
        });
    }

    function getNewDealJSON(formPostUrl) {
        var template = $('#singleCreateDealDiv').html();
        $.getJSON(formPostUrl, function (data) {
            var template = $('#merchantCreateDealsListDiv').html('');

            var output = Mustache.render(template, data);
            $('#merchantCreateDealsListDiv').prepend(output);
        });
    }

    function getEditDealMerchantJSON(formPostUrl) {
        $.getJSON(formPostUrl, function (data) {
            $("#title").attr('value', data.title);
            $("#quantity").attr('value', data.quantity);
            $("#priorPrice").attr('value', data.priorPrice);
            $("#curPrice").attr('value', data.curPrice);
            $("#startDate").attr('value', data.startDate);
            $("#endDate").attr('value', data.endDate);
            $("#description").text(data.description);


        });
    }

    function displayCreateAdDealForm() {
        $("#registerdPost").show();
        $("#addDealStatus").hide();

    }


    /*function editDealAjax(formPostUrl){
     var id=7;
     //        var dataString = $("#username").val();
     $.ajax({
     type: "POST",
     url:formPostUrl ,
     data:{id:id},
     dataType: "json",
     success: function(data) {
     if(data.resultData=='true'){


     }
     },error: function(){

     }
     });


     }*/
    $("#addDealsLink").click(function () {
        displayCreateAdDealForm();
    });

    function getPurchaseHistoryList(formPostUrl) {
        var template = $('#showBuyers').html();
        $('#merchantCreateDealsListDiv').empty();
        $.getJSON(formPostUrl, function (data) {
//            console.log(".....");
//            alert(data.length);
           if(data.length > 0){
            $.each(data, function(idx, obj){
                console.log("id is"+obj.id);
                console.log("buyerListURL is"+obj.buyerListURL);
                var output = Mustache.render(template,obj);

                $('#merchantCreateDealsListDiv').prepend(output);
            });
//            $("#backBtn").show();
           }
            else{
//               alert('not have data');
               $("#merchantCreateDealsListDiv").append("<p class='bold_red_text'><span >Sorry Buyers Not avail , For this Add </span > <a href='javascript:void(0)' class='btn btn-warning' onclick='goBack2()' style='color: #7c1212'> Back </a></p>");
           }
           });
    }
 function goBack2(){
//     alert('go back');
     getMerchantDealsJSON("${createLink(controller: 'deal',action: 'getMerchantsDealJSON')}");
 }

    function addPost(){
        displayCreateAdDealForm();
    }
    function getDealDetailsJSON(formPostUrl){
//        alert('hello');
        var template = $('#showDetails').html();
        $('#showDetailsDiv').empty();
        $.getJSON(formPostUrl, function (data) {
//            console.log(".....");
//            alert(''+data.length);
               var output=Mustache.render(template,data);
            $('#showDetailsDiv').append(output);
            $(".landingPageForms").hide();
            $("#dealDetails").show();
        });


    }
</script>
