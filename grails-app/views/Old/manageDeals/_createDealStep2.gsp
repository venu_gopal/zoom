<div id="step-2" class="tab-pane fade in active br-lblue" style="height: 400px">

    <div class="form-group">
        <div class="col-sm-12">
            <label for="quantity" class="youmail" data-icon="m">Quantity</label>
            <input id="quantity" class="form-control" name="quantity" required="required" type="text"/>
            <input type="checkbox" class="" alt="unlimited" name="unlimited"> Unlimited
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <label for="priorPrice" class="youmail" data-icon="m">Prior Price</label>
            <input id="priorPrice" class="form-control" name="priorPrice" required="required" type="text"/>
        </div>
    </div>


    <div class="form-group">
        <div class="col-sm-12">
            <label for="curPrice" class="youmail" data-icon="m">Current Price</label>
            <input id="curPrice" class="form-control" name="curPrice" required="required" type="text"/>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <label for="startDate" class="youmail" data-icon="m">Start Date</label>
            <input id="startDate" class="form-control datepicker" name="startDate" required="required" type="text"/>
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-12">
            <label for="endDate" class="youmail" data-icon="m">End Date</label>
            <input id="endDate" class="form-control small datepicker" name="endDate" required="required" type="text"/>
        </div>
    </div>
</div>