<div id="step-1" class="tab-pane fade in active br-brown" style="height:400px">

    <div class="form-group">
        <label for="title" class="youmail" data-icon="m">Title</label> <br>

        <div class="col-sm-12">
            <input id="title" name="title" class="form-control" required="required" type="text"/>
        </div>
    </div>


    <div class="form-group">
        <label for="description" class="youpasswd" data-icon="n">Description</label> <br>

        <div class="col-sm-12">
            <textarea id="description" name="description" style="width: 660px; margin: 0px; height: 138px;"></textarea>
        </div>
    </div>

    <div class="form-group">
        <label for="logo" class="youpasswd" data-icon="n">Logo</label> <br>

        <div class="col-sm-12">
            <input id="logo" class="form-control" name="logo" required="required" type="file"/>
        </div>
    </div>

</div>
