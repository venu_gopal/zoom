<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="public"/>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>

    <script src="${resource(dir: 'js', file: 'google/gmaps.js', absolute: true)}"></script>
</head>

<body>
<div class="wrapper white">
    <div class="inner-page">
        <div class="container">

            <div id="dealsDisplay" class="landingPageForms" style="display:none">
                <g:render template="/public/showDealsCompl"/>
            </div>
            <div id="dealsD" class="landingPageForms" style="display:none">
                <g:render template="/public/showAllBuyersList"/>
            </div>

            <div id="merchantDisplay" class="landingPageForms" style="display:none">
                <div class="page-mainbar login">
                    <g:render template="/merchant/registerNewCompl"/>
                </div>

            </div>

            <div id="manageDealDisplay" class="landingPageForms" style="display:none">
                <div class="page-mainbar login col-md-12" style="padding-left: 0">
                    <div class="col-md-6">
                        <ul id="merchantCreateDealsListDiv" style="position: relative;padding-left: 0;">

                        </ul>
                        %{--<div id="backBtn">--}%
                            %{--<li style="display: inline-table">--}%
                                %{--<a href="javascript:void(0)"--}%
                                   %{--onclick="getDealsJSON('${createLink(controller: 'publicApi',action: 'getMerchantsDealJSON')}')" class="btn btn-danger">Back</a>--}%
                            %{--</li>--}%
                        %{--</div>--}%
                    </div>
                    %{--<div class="login-content">--}%
                        %{--<div class="tab-content">--}%
                            <div class="login-content col-md-6"
                                 style="max-width: 700px;padding-top: 0px;padding-right: 0;margin-top: 0px">
                                <g:render template="/manageDeals/createDealComp"/>
                            </div>
                        %{--</div>--}%
                    %{--</div>--}%
                </div>
            </div>

            <div id="shopperDisplay" class="landingPageForms" style="display: none">
                <div class="page-mainbar login">
                    <div class="login-content">
                        <div class="tab-content">
                            <g:render template="/shopper/shopperRegistration"/>
                        </div>
                        <br/>
                        <br/>

                        <div class="span12" style="float: right">
                            Already a member ? <a href="javascript:void(0)" id="tologin" class="tologin">Login Here</a>
                        </div>
                    </div>
                </div>
            </div>

            <div id="signUpDisplay" class="landingPageForms" style="display: none">
                <div class="page-mainbar login">
                    <div class="login-content">
                        <div class="tab-content">
                            <g:render template="/templates/login"/>
                        </div>

                    </div>
                </div>
            </div>
            <div id="purchaseDealsDisplay" class="landingPageForms" style="display: none">
                <div class="page-mainbar login">
                    <div class="">
                        <div class="tab-content">
                            <g:render template="/shopper/allPurchaseDeals"/>
                        </div>

                    </div>
                </div>
            </div>
            <div id="dealDetails" class="landingPageForms" style="display: none">
                <div class="page-mainbar dealDetails">
                    <div class="">
                        <div class="tab-content">
                            <g:render template="/public/dealDetails"/>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">


    $("#shopper").click(
            function () {
//                $('#registerShopper').load(window.location.pathname+' #registerShopper');
                $(".landingPageForms").hide();
                $("#shopperDisplay").show();
            }
    );

    $(".shopperSignup").click(
            function () {
                $(".landingPageForms").hide();
                $("#shopperDisplay").show();
            }
    );
    $(".toregister").click(
            function () {
                $(".landingPageForms").hide();
                $("#shopperDisplay").show();
            }
    );
    $("#login").click(
            function () {
                $(".landingPageForms").hide();
                $("#signUpDisplay").show();
            }
    );
    $("#loginTo").click(
            function () {
                $(".landingPageForms").hide();
                $("#signUpDisplay").show();
            }
    );
    $(".tologin").click(
            function () {
                $(".landingPageForms").hide();
                $("#signUpDisplay").show();
            }
    );
    $(".manageDeal").click(
            function () {
                $(".landingPageForms").hide();
                $("#manageDealDisplay").show();
            }
    );
    $("#deals").click(
            function () {
                $(".landingPageForms").hide();
                $("#dealsDisplay").show();
            }
    );
    $("#merchant").click(
            function () {
                $(".landingPageForms").hide();
                $("#merchantDisplay").show()
            }
    );
    $("#purchaseDeals").click(
            function(){
                %{--getAllPurchasePosts('${createLink(controller:'publicApi',action:'getPurchaseDealList')}');--}%
                $(".landingPageForms").hide();
                $("#purchaseDealsDisplay").show("slow");

            });

    function verifyEmailValidity(formPostUrl) {
        var dataString = $("#username").val();
        console.log(dataString);
        $.ajax({
            type: "POST",
            url: formPostUrl,
            data: {email: dataString},
            dataType: "json",
            success: function (data) {
                if (data.resultData == 'true') {
                    $('.showEmailValidationFail').show();
                    $('.buttonNext').addClass('buttonDisabled');
                } else {
                    $('.showEmailValidationFail').hide();
                    $('.buttonNext').removeClass('buttonDisabled');
                }
            }, error: function () {
                $('.buttonNext').addClass('buttonDisabled');
            }
        });
    }


</script>
</body>
</html>