<!doctype html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="ZoomDeals"/></title>
    %{--<meta name="viewport" content="width=device-width, initial-scale=1.0">--}%
    %{--<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">--}%
    %{--<link rel="apple-touch-icon" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">--}%
    %{--<link rel="apple-touch-icon" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">--}%
    %{--<link rel="stylesheet" href="${resource(dir: 'css', file: 'main.css')}" type="text/css">--}%
    %{--<link rel="stylesheet" href="${resource(dir: 'css', file: 'mobile.css')}" type="text/css">--}%

    <meta name="description" content="Your description">
    <meta name="keywords" content="Your,Keywords">
    <meta name="author" content="ResponsiveWebInc">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
    <!-- Styles -->
    <!-- Bootstrap CSS -->
    <link href="${resource(dir: 'bootstrap-theme', file: 'css/bootstrap.min.css')}" rel="stylesheet">
    <!-- Animate CSS -->
    <link href="${resource(dir: 'bootstrap-theme', file: 'css/animate.min.css')}" rel="stylesheet">
    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link href="${resource(dir: 'bootstrap-theme', file: 'css/settings.css')}" rel="stylesheet">
    <!--[if IE ]><link rel="stylesheet" href="${resource(dir: 'bootstrap-theme', file: 'css/settings-ie8.css')}"><![endif]-->
    <!-- Portfolio CSS -->
    <link href="${resource(dir: 'bootstrap-theme', file: 'css/prettyPhoto.css')}" rel="stylesheet">
    <!-- Countdown CSS -->
    <link href="${resource(dir: 'bootstrap-theme', file: 'css/jquery.countdown.css')}" rel="stylesheet">
    <!-- Font awesome CSS -->
    <link href="${resource(dir: 'bootstrap-theme', file: 'css/font-awesome.min.css')}" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="${resource(dir: 'bootstrap-theme', file: 'css/style.css')}" rel="stylesheet">
    <!--[if IE]><link rel="stylesheet" href="${resource(dir: 'bootstrap-theme', file: 'css/ie-style.css')}"><![endif]-->

    <!-- Favicon -->
    <link rel="shortcut icon" href="#">
    <script src="${resource(dir: 'bootstrap-theme', file: 'js/jquery.js')}"></script>
    <!-- Bootstrap JS -->
    <script src="${resource(dir: 'bootstrap-theme', file: 'js/bootstrap.min.js')}"></script>

    %{--<script src="//code.jquery.com/jquery-1.9.1.js"></script>--}%
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    %{----}%
    %{--<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>--}%
    %{--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>--}%
    %{--<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=true"></script>--}%
    %{--<script type="text/javascript" src="PATH_TO_PLUGIN/jquery.ui.map.js"></script>--}%
    %{--<link rel="stylesheet" href="/resources/demos/style.css">--}%


    <g:layoutHead/>
    <r:layoutResources/>
</head>

<body>

<!-- Body Wrapper -->
<div class="wrapper white">

<!-- Header Start -->
<div class="header">
<!-- Header Information -->
<div class="header-info">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <!-- Social Media -->
                <div class="social">
                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                </div>
            </div>

            <div class="col-md-4 col-sm-4">
                <!-- Search Box -->
                <div class="search">
                    <form>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Text to search">
                            <span class="input-group-btn">
                                <button class="btn" type="button"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
            </div>

            <div class="col-md-4 col-sm-4">
                <!-- Contact Info -->
                <div class="contact-info">
                    <span>
                        <p id="loginAs"><a href="javascript:void(0)" %{--id="loginTo"--}% class="tologin">Login</a></p>
                        %{--<sec:ifLoggedIn>
                            Logged in as <sec:username/> (<g:link controller='logout'>Logout</g:link>)
                        </sec:ifLoggedIn>
                        <sec:ifNotLoggedIn>
                            <a href='${createLink(controller: 'public', action: 'loginAjax')}'>Login</a>
                        </sec:ifNotLoggedIn>--}%

                        %{--<i class="fa fa-phone br-lblue"></i> <strong>+91 234 - 234 - 3231</strong></span>--}%
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Header Navigation -->
<div class="header-navigation">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <!-- Logo -->
                <div class="logo">
                    <!-- Heading -->
                    <h1><a href="${createLink(uri: '/')}"><i class="fa fa-eye-slash br-red"></i> Zoom Deals</a></h1>
                </div>
            </div>

            <div class="col-md-8">
                <!-- Bootstrap Navbar -->
                <nav class="navbar navbar-default" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle br-orange" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <a href="index.html" class="br-orange">
                                    <!-- Link Icon -->
                                    <i class="fa fa-home link-icon"></i>
                                    <!-- Link Title -->
                                    <span class="link-title">Home</span>
                                </a>
                            </li>
                            %{-- <li class="dropdown">
                                 <a href="#" class="dropdown-toggle br-purple" data-toggle="dropdown">
                                     <!-- Link Icon -->
                                     <i class="fa fa-file link-icon"></i>
                                     <!-- Link Title -->
                                     <span class="link-title">Page 1 <b class="fa fa-angle-down"></b></span>
                                 </a>
                                 <ul class="dropdown-menu dropdown-sm">
                                     <li>
                                         <div class="row">
                                             <div class="col-md-6 col-sm-6">
                                                 <div class="col-inner col-disable">
                                                     <h4 class="br-orange"><i class="fa fa-share heading-icon"></i> Suffered Inaltera</h4>
                                                     <p>There are many variations of box passages of Lorem Ipsum blackly available, but the majority have non suffered in alteration in some one form, by injected humour, or book randomised even slightly forms believable.</p>
                                                 </div>
                                             </div>
                                             <div class="col-md-6 col-sm-6">
                                                 <div class="col-inner">
                                                     <ul class="list-unstyled">
                                                         <li><a href="error.html"><i class="fa fa-arrow-right dd-link-icon"></i> 404 Error</a></li>
                                                         <li><a href="coming-soon.html"><i class="fa fa-arrow-right dd-link-icon"></i> Coming Soon</a></li>
                                                         <li><a href="feature.html"><i class="fa fa-arrow-right dd-link-icon"></i> Features</a></li>
                                                         <li><a href="gallery.html"><i class="fa fa-arrow-right dd-link-icon"></i> Gallery</a></li>
                                                         <li><a href="login.html"><i class="fa fa-arrow-right dd-link-icon"></i> Login</a></li>
                                                         <li><a href="login.html"><i class="fa fa-arrow-right dd-link-icon"></i> Register</a></li>
                                                     </ul>
                                                 </div>
                                             </div>
                                         </div>
                                     </li>
                                 </ul>
                             </li>--}%
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle br-lblue" data-toggle="dropdown">
                                    <!-- Link Icon -->
                                    <i class="fa fa-file link-icon"></i>
                                    <!-- Link Title -->
                                    <span class="link-title">Page 2 <b class="fa fa-angle-down"></b></span>
                                </a>
                                <ul class="dropdown-menu dropdown-md">
                                    <li>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4">
                                                <div class="col-inner col-disable">
                                                    <!-- Heading -->
                                                    <h4 class="br-green"><i
                                                            class="fa fa-filter heading-icon"></i> Varsa Oukes</h4>
                                                    <!-- Paragraph -->
                                                    <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui app blanditiis praesentium voluptatum deleniti atque corrupti quos pain dolores et quas molesti as excet pturi sint occaecati cupiditate non provident.</p>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-sm-4">
                                                <div class="col-inner col-disable">
                                                    <!-- Heading -->
                                                    <h4 class="br-yellow"><i
                                                            class="fa fa-ticket heading-icon"></i> Slightly Belie</h4>
                                                    <!-- Image -->
                                                    <img src="${resource(dir: 'bootstrap-theme', file: 'img/w2.jpg')}"
                                                         class="img-responsive" alt=""/>
                                                    <!-- Paragraph -->
                                                    <p>Lorem Ipsum available, but the cool majority have suffered altevable.</p>
                                                    <!-- View Next Button -->
                                                    <div class="view-button">
                                                        <a href="#" class="btn btn-danger btn-sm">View More <i
                                                                class="fa fa-angle-right"></i></a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-sm-4">
                                                <div class="col-inner">
                                                    <ul class="list-unstyled">
                                                        <li><a href="blog.html"><i
                                                                class="fa fa-arrow-right dd-link-icon"></i> Blog</a>
                                                        </li>
                                                        <li><a href="blog-single.html"><i
                                                                class="fa fa-arrow-right dd-link-icon"></i> Single Blog
                                                        </a></li>
                                                        <li><a href="portfolio.html"><i
                                                                class="fa fa-arrow-right dd-link-icon"></i> Portfolio
                                                        </a></li>
                                                        <li><a href="pricing.html"><i
                                                                class="fa fa-arrow-right dd-link-icon"></i> Pricing</a>
                                                        </li>
                                                        <li><a href="product.html"><i
                                                                class="fa fa-arrow-right dd-link-icon"></i> Products</a>
                                                        </li>
                                                        <li><a href="testimonial.html"><i
                                                                class="fa fa-arrow-right dd-link-icon"></i> Testimonial
                                                        </a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                            <li>
                                %{--<a href="${createLink(controller: 'public', action: 'showDeals')}" id="deals" class="br-red">--}%
                                <a href="javascript:void(0)" id="deals" class="br-red publicMenu">
                                    <!-- Link Icon -->
                                    <i class="fa fa-user link-icon"></i>
                                    <!-- Link Title -->
                                    <span class="link-title">All Deals</span>
                                </a>
                            </li>
                            <li>
                                %{--<a href="${createLink(controller: 'public', action: 'loginAjax')}" id="login" class="br-yellow">--}%
                                <a href="javascript:void(0)" id="login" class="br-yellow publicMenu">
                                    <!-- Link Icon -->
                                    <i class="fa fa-phone link-icon"></i>
                                    <!-- Link Title -->
                                    <span class="link-title">Login</span>
                                </a>
                            </li>
                            <li>
                                %{--<a href="${createLink(controller: 'public', action: 'loginAjax')}" id="login" class="br-yellow">--}%
                                <a href="${createLink(controller: 'logout')}" id="logout" class="br-red publicMenu" style="display: none">
                                    <!-- Link Icon -->
                                    <i class="fa fa-phone link-icon"></i>
                                    <!-- Link Title -->
                                    <span class="link-title">Logout</span>
                                </a>
                            </li>
                            <li>
                                %{--<a href="${createLink(controller: 'merchant', action: 'signUp')}" id="merchant" class="br-brown">--}%
                                <a href="javascript:void(0)" id="merchant" class="br-brown publicMenu">
                                    <!-- Link Icon -->
                                    <i class="fa fa-user link-icon"></i>
                                    <!-- Link Title -->
                                    <span class="link-title">Merchant</span>
                                </a>
                            </li>
                            %{--<sec:ifLoggedIn>--}%

                            <li id="manageDealIcon" style="display: none">
                                %{--<a href="${createLink(controller: 'deal', action: 'createDeal')}" id="manageDeal" class="br-blue">--}%
                                <a href="javascript:void(0)" id="manageDeal" class="br-blue publicMenu manageDeal">
                                    <!-- Link Icon -->
                                    <i class="fa  link-icon"></i>
                                    <!-- Link Title -->
                                    <span class="link-title">Manage Deals</span>
                                </a>
                            </li>
                            %{--</sec:ifLoggedIn>--}%

                            <li>
                                %{--<a href="${createLink(controller: 'shopper', action: 'signUp')}" class="br-green" id="shopper">--}%
                                <a href="javascript:void(0)" class="br-green publicMenu" id="shopper">
                                    <!-- Link Icon -->
                                    <i class="fa  link-icon"></i>
                                    <!-- Link Title -->
                                    <span class="link-title">Shopper</span>
                                </a>
                            </li>
                            <li>
                                %{--<a href="${createLink(controller: 'shopper', action: 'signUp')}" class="br-green" id="shopper">--}%
                                <a href="javascript:void(0)" class="br-purple publicMenu" id="purchaseDeals" style="display: none">
                                    <!-- Link Icon -->
                                    <i class="fa  link-icon"></i>
                                    <!-- Link Title -->
                                    <span class="link-title">Purchase Deals</span>
                                </a>
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </nav>
            </div>
        </div>
    </div>
</div>
</div>



%{--<div id="grailsLogo" role="banner"><a href="http://grails.org"><img src="${resource(dir: 'images', file: 'grails_logo.png')}" alt="Grails"/></a></div>--}%
<g:layoutBody/>
%{--<div class="footer" role="contentinfo"></div>--}%
%{--<div id="spinner" class="spinner" style="display:none;"><g:message code="spinner.alt" default="Loading&hellip;"/></div>--}%
%{----}%

</div>

<!-- Footer End -->

<!-- Javascript files -->
<!-- jQuery -->
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script src="${resource(dir: 'js', file: 'google/gmaps.js', absolute: true)}"></script>

<script type="text/javascript"
        src="${resource(dir: 'bootstrap-theme', file: 'js/jquery.themepunch.plugins.min.js')}"></script>
<script type="text/javascript"
        src="${resource(dir: 'bootstrap-theme', file: 'js/jquery.themepunch.revolution.min.js')}"></script>
<!-- Cycle JS -->
<script type="text/javascript" src="${resource(dir: 'bootstrap-theme', file: 'js/jquery.cycle.all.js')}"></script>
<!-- jQuery flot -->
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="${resource(dir: 'bootstrap-theme', file: 'js/excanvas.min.js')}"></script><![endif]-->
<script src="${resource(dir: 'bootstrap-theme', file: 'js/jquery.flot.min.js')}"></script>
<script src="${resource(dir: 'bootstrap-theme', file: 'js/jquery.flot.resize.min.js')}"></script>
<!-- Count To JS  -->
<script src="${resource(dir: 'bootstrap-theme', file: 'js/jquery.countTo.js')}"></script>
<!-- jQuery way points -->
<script src="${resource(dir: 'bootstrap-theme', file: 'js/waypoints.min.js')}"></script>
<!-- jQuery prettyPhoto & Isotope -->
<script src="${resource(dir: 'bootstrap-theme', file: 'js/jquery.prettyPhoto.js')}"></script>
<script src="${resource(dir: 'bootstrap-theme', file: 'js/isotope.js')}"></script>
<!-- Jquery for Countdown  -->
<script type="text/javascript" src="${resource(dir: 'bootstrap-theme', file: 'js/jquery.countdown.min.js')}"></script>
<!-- Respond JS for IE8 -->
<script src="${resource(dir: 'bootstrap-theme', file: 'js/respond.min.js')}"></script>
<!-- HTML5 Support for IE -->
<script src="${resource(dir: 'bootstrap-theme', file: 'js/html5shiv.js')}"></script>
<!-- Custom JS -->
<script src="${resource(dir: 'bootstrap-theme', file: 'js/custom.js')}"></script>

<script type="text/javascript" src="${resource(dir: 'js', file: 'mustache/mustache.js')}"></script>

<script src="${resource(dir: "bootstrap-theme", file: "js/jquery.colorbox.js", absolute: true)}"></script>
%{--<script src="${resource(dir: "bootstrap-theme", file: "js/jquery.colorbox-min.js", absolute: true)}"></script>--}%

<link rel="stylesheet" type="text/css"
      href="${resource(dir: 'css', file: 'colorbox/colorbox.css', absolute: true)}"/>

<link rel="stylesheet" type="text/css"
      href="${resource(dir: 'css', file: 'smartwizard/smart_wizard.css', absolute: true)}"/>


<script src="${resource(dir: "js", file: "smartwizard/jquery.smartWizard-2.0.js", absolute: true)}"></script>
<script src="${resource(dir: 'js', file: 'metroui/dialog.js', absolute: true)}"></script>
<script src="${resource(dir: 'js', file: 'jquery/mustache.js', absolute: true)}"></script>
<script type="text/javascript" src="${resource(dir: 'js', file: 'jquery/jquery-validator-1.11.1.js')}"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">

<!-- This Page JavaScript -->
<script type="text/javascript">

    // SLIDER REVOLUTION Java Script
    jQuery(document).ready(function () {

        jQuery('.tp-banner').revolution(
                {
                    delay: 9000,
                    startheight: 500,

                    hideThumbs: 10,

                    navigationType: "none",


                    hideArrowsOnMobile: "on",

                    touchenabled: "on",
                    onHoverStop: "on",

                    navOffsetHorizontal: 0,
                    navOffsetVertical: 20,

                    stopAtSlide: -1,
                    stopAfterLoops: -1,

                    shadow: 0,

                    fullWidth: "on",
                    fullScreen: "off"
                });
    });

    // Cycle Slide Js

    $('#box-slider-one').cycle({
        fx: 'uncover',
        speed: 200,
        timeout: 2500
    });

    /* ******************************************** */
    /* plot js */
    /* *************************************** */

    $(function () {
        /* Chart data #1 */
        var d1 = [
            [0, 0],
            [1, 1.5],
            [2, 3.2],
            [3, 4.1],
            [4, 5.1],
            [5, 5],
            [6, 5.3],
            [7, 6],
            [8, 6.3],
            [9, 7],
            [10, 7.5],
            [11, 8]
        ];

        var options = {
            series: {
                lines: {
                    show: true, fill: false, lineWidth: 1
                },
                points: {
                    show: true, fill: true, lineWidth: 2, radius: 3, fillColor: "#fff"
                },
                shadowSize: 0
            },
            colors: ["#fff"],
            grid: {
                hoverable: true, color: "#fff", backgroundColor: null, borderWidth: 0, borderColor: "#fff", labelMargin: 10
            },
            xaxis: {
                ticks: 10,
                font: {
                    size: 12,
                    color: ["#fff"]
                }
            },
            yaxis: {
                ticks: 5,
                font: {
                    size: 12,
                    color: ["#fff"]
                }
            },
            legend: {
                backgroundOpacity: 0,
                noColumns: 2,
                labelBoxBorderColor: null
            }
        };

        $("<div id='tooltip'></div>").css({
            position: "absolute",
            display: "none",
            "border-radius": "1px",
            padding: "4px 5px",
            color: "#999",
            "font-size": "11px",
            "background-color": "#fff",
            "border": "1px solid #ccc",
            "z-index": "20"
        }).appendTo("body");

        $(".plot-chart").bind("plothover", function (event, pos, item) {
            if (item) {
                var x = item.datapoint[0].toFixed(2),
                        y = item.datapoint[1].toFixed(2);

                $("#tooltip").html(x + ", " + y)
                        .css({top: item.pageY + 5, left: item.pageX + 5})
                        .fadeIn(200);
            } else {
                $("#tooltip").hide();
            }
        });

        $.plot(".plot-chart", [
            {data: d1, label: "Revenue"}
        ], options);
    });

    // Type your codde here
</script>

<r:layoutResources/>
<footer id="footer">
    <g:render template="/layouts/footer"/>

</footer>

</body>
</html>