﻿<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <title><g:layoutTitle default="Zoom Deals"/></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Modern UI CSS">
    <meta name="author" content="Alpha Sigma">
    <meta name="keywords" content="best deals, australia, realtime">

    <link rel="stylesheet" type="text/css" href="${resource(dir: 'css', file: 'menubar.css', absolute: true)}"/>
    <script src="<g:resource dir="js" file="jquery/jquery-1.9.0.js" absolute="true"/>"></script>
    <g:layoutHead/>

</head>

<body>
<g:layoutBody/>

</body>
</html>