<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<!-- Title here -->
		<title>Deals - Haggell</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Your description">
		<meta name="keywords" content="Your,Keywords">
		<meta name="author" content="ResponsiveWebInc">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
      <!-- Animate css -->
      <link href="css/animate.min.css" rel="stylesheet">
      <!-- Dropdown menu -->
      <link href="css/ddlevelsmenu-base.css" rel="stylesheet">
      <link href="css/ddlevelsmenu-topbar.css" rel="stylesheet">
      <!-- Countdown -->
      <link href="css/jquery.countdown.css" rel="stylesheet"> 
		<!-- Font awesome CSS -->
		<link href="css/font-awesome.min.css" rel="stylesheet">		
		<!-- Custom CSS -->
		<link href="css/style.css" rel="stylesheet">
		<link href="css/liteaccordion.css" rel="stylesheet" />
		<link href="css/datepicker.css" rel="stylesheet">
		<!-- Metro CSS -->
		<link href="css/metro-bootstrap.css" rel="stylesheet">
		<link href="css/metro-bootstrap-responsive.css" rel="stylesheet"> 
		<!-- Favicon -->
		<link rel="shortcut icon" href="#">
		 <!-- jQuery -->
        <script src="js/jquery.min.js"></script>

        <!-- easing -->
        <script src="js/jquery.easing.1.3.js"></script>

        <!-- liteAccordion js -->
        <script src="js/liteaccordion.jquery.js"></script>
	</head>                
	
	<body>

      <!-- Shopping cart Modal -->
     <div class="modal fade" id="shoppingcart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 class="modal-title">Deals</h4>
           </div>
           
           <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal">Continue Shopping</button>
             <button type="button" class="btn btn-info">Checkout</button>
           </div>
         </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->
           
      <!-- Logo & Navigation starts -->
      
      <div class="header">
		<div class="container">
		 <div class="row">
		  <div class="col-md-2 col-sm-2">
		  <!-- Logo -->
			<div class="logo">
			  <h1><a href="index.html"><img src="img/logo.png"/></a></h1>
			</div>
			</div>
				<div class="col-md-8 col-sm-5">
				<!-- Navigation menu -->
				<div class="navi">
				  <div class="mattblackmenu">
					<ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Start Selling</a></li>
						<li><a href="login.html">Signin</a></li>
						<li><a href="register.html">Join</a></li>
						<li><a href="account.html">My profile</a></li>
						<li><a href="#">My Sales</a></li>
						<li><a href="orderhistory.html">My Purchases</a></li>
						<li><a href="contactus.html">Contact us</a></li>
						</ul>
					</div>
					</div>
					<!-- Dropdown NavBar -->
					<div class="navis"></div>                   
					</div>
					<div class="col-md-2 col-sm-5">
					 <div class="kart-links pull-right">							
						<a data-toggle="modal" href="#shoppingcart"><i class="fa fa-shopping-cart"></i> 3 Items - $300</a>
						</div>
					</div>
				</div>
			</div>
		</div>
      
      <!-- Logo & Navigation ends -->
     
      <!-- Page title -->
      <div class="page-title">
         <div class="container">
            <h2><i class="fa fa-user color"></i>Deals</h2>
            <hr />
         </div>
      </div>
      <!-- Page title -->
      
      <!-- Page content -->
		  
		  <div class="shop-items">
			<div class="container">
			   <div class="row">
				  <div id="one">
					<ol>
						<li  data-slide-name="slide-first">
						  <h2><span>Step One</span></h2>
							<div>
								<figure>
								  <div class="col-sm-4">
									<input type="text" class="form-control input-bg3" placeholder="Title"></input>
									</div>
									<div class="clearfix"></div>
									<div class="col-sm-9">
									  <textarea class="form-control input-bg3" rows="7" placeholder="Message">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s.</textarea>
									  </div>
									  <div class="clearfix"></div>
									<input type="file" title="Choose Logo" class="btn-primary upload-txt">
									<a href="#slide-second"><figcaption class="ap-caption">Next <i class="fa fa-angle-double-right"></i></figcaption></a>
								</figure>
							</div>
						  </li>
						<li  data-slide-name="slide-second">
							<h2><span>Step Two</span></h2>
							<div class="col-sm-6">
								<div class="txt-part-acc">
								<div class="col-sm-6">
								 <div class="col-sm-5">
									  <label>Quantity Available</label>
									  </div>
									  <div class="col-sm-6">
									  <input type="number" class="form-control input-bg2"></input>
									  </div>
									  <div class="col-sm-1"></div>
									 <div class="col-sm-5">
									  <label>Normal Price</label>
									  </div>
									  <div class="col-sm-6">
									  <label class="price-txt">$360</label>
									  </div>
									  <div class="col-sm-1"></div>
									  <div class="col-sm-5">
									  <label>Discounted Price</label>
									  </div>
									  <div class="col-sm-6">
									   <label class="price-txt">$300</label>
									  </div>
									  <div class="col-sm-1"></div>
									  <div class="clearfix"></div>
									  
									<div id="datetimepicker" class="input-append date">
									<div class="col-sm-5">
									  <label>Start Date</label>
									  </div>
									  <div class="col-sm-6">
									  <input type="text" class="form-control input-bg"></input>
									  </div>
									  <div class="col-sm-1">
									  <span class="add-on">
										<i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
									  </span>
									  </div>
									  <div class="col-sm-5">
									  <label>End Date</label>
									  </div>
									  <div class="col-sm-6">
									  <input type="text" class="form-control input-bg"></input>
									  </div>
									  <div class="col-sm-1">
									  <span class="add-on">
										<i class="fa fa-calendar" data-time-icon="icon-time" data-date-icon="icon-calendar"></i>
									  </span>
									  </div>
									  </div>
									  </div>
									<a href="#slide-third"><figcaption class="ap-caption">Next <i class="fa fa-angle-double-right"></i></figcaption></a>
								</div>
							</div>
						</li>
						<li data-slide-name="slide-third">
							<h2><span>Step Three</span></h2>
							<div>
								<figure>
								<h4 class="head-term-con">Address</h4>
								<p class="acc-pra-txt"> 2, Plot No.14, Raj Karmara Street,<br>
							     5th Stage, Koramangala, Madiwala,<br>
									  Bangalore - 493922, Karananana.<br></p>
									<img src="img/staticmap.png" class="map-sty" alt=""/>
								 <a  href="#slide-fourth"><figcaption class="ap-caption">Next <i class="fa fa-angle-double-right"></i></figcaption></a>
								</figure>
							</div>
						</li>
						<li data-slide-name="slide-fourth">
						<h2><span>Step Four</span></h2>
							<div>
							 <figure>
								 <div class="col-sm-4">
									  <input type="text" class="form-control input-bg3" placeholder="Title" readonly></input>
									</div>
									<div class="clearfix"></div>
									<div class="col-sm-9">
									  <textarea class="form-control input-bg3" rows="7" placeholder="Message" readonly>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s.
									  </textarea>
									  </div>
									  <div class="clearfix"></div>
									<a href="#"><figcaption class="ap-caption"><i class="fa fa-check"></i> Finish</figcaption></a>
								</figure>
							</div>
						</li>
						
					</ol>
           
        </div>
		
        <!-- liteAccordion demos, social & analytics -->
        <script>
            (function($, d) {
                // please don't copy and paste this page
                // it breaks my analytics!

                // demos
                $('#one').liteAccordion({
                        onTriggerSlide : function() {
                            this.find('figcaption').fadeOut();
                        },
                        onSlideAnimComplete : function() {
                            this.find('figcaption').fadeIn();
                        },
                        autoPlay : false,
                        pauseOnHover : true,
                        theme : 'stitch',
                        rounded : true,
                        enumerateSlides : true,
						linkable :true,
                }).find('figcaption:first').show();
                $('#two').liteAccordion( {linkable :true,});
                $('#three').liteAccordion({ theme : 'dark', rounded : true, enumerateSlides : true, firstSlide : 2, linkable : true, easing: 'easeInOutQuart' });
                $('#four').liteAccordion({ theme : 'light', firstSlide : 3, easing: 'easeOutBounce', activateOn: 'mouseover',linkable :true, });

                // social links
                var js, id = 'facebook-jssdk';

                if (d.getElementById(id)) return;

                js = d.createElement('script');
                js.id = id;
                js.async = true;
                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                d.getElementsByTagName('head')[0].appendChild(js);

                // Load Plus One Button
                jQuery.getScript('https://apis.google.com/js/plusone.js');
                // Load Tweet Button Script
                jQuery.getScript('https://platform.twitter.com/widgets.js');
                // Load LinkedIn button
                jQuery.getScript('https://platform.linkedin.com/in.js?v=2');
            })(jQuery, document);

            // analytics
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-17442910-1']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
               
				<!-- <div class="sep"></div>  --> 
				 </div>
				</div>  <!-- Recent posts Starts -->
			 
			  <!-- Recent posts Starts -->
					
			  <!-- Recent posts Ends -->		
			  
			  
			  <!-- Catchy starts -->
				   
			  <!-- Catchy ends -->
			  
			  <!-- CTA Starts -->
			  
			  <!-- CTA Ends -->
      
      <!-- Footer starts -->
      <footer>
       <div class="container">
         <div class="row">
			<div class="col-md-4 col-sm-4">
              <div class="fwidget">
                <h4>About Us</h4> 
                  <hr />
                  <p>Duis leo risus, vehicula luctus nunc.  Quiue rhoncus, a sodales enim arcu quis turpis. Duis leo risus, condimentum ut posuere ac, vehicula luctus nunc.  Quisque rhoncus, a sodales enim arcu quis turpis.</p>
                  <div class="social">
                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                    <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    <a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
                  </div>
                </div>
              </div>
              <div class="col-md-4 col-sm-4">
               <div class="fwidget">
                <h4>Categories</h4>
                 <hr />
                    <ul>
						<li><a href="#">Home</a></li>
						<li><a href="#">Start Selling</a></li>
						<li><a href="login.html">Signin</a></li>
						<li><a href="register.html">Join</a></li>
						<li><a href="account.html">My profile</a></li>
						<li><a href="#">My Sales</a></li>
						<li><a href="orderhistory.html">My Purchases</a></li>
						<li><a href="contactus.html">Contact us</a></li>
					</ul>
                   </div>
                  </div> 
				  
				  <div class="col-md-4 col-sm-4">
                      <div class="fwidget">
                       <h4>Get In Touch</h4>
                           <hr />
                           <div class="address">
                              <p><i class="fa fa-home color contact-icon"></i> #12, Plot No.14, Raj Karmara Street, </p>
                              <p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 5th Stage, Koramangala, Madiwala,</p>
                              <p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Bangalore - 493922, Karananana.</p>
                              <p><i class="fa fa-phone color contact-icon"></i> +94-948-323-5532</p>
                              <p><i class="fa fa-envelope color contact-icon"></i> <a href="mailto:something@gmail.com">info@olsonkart.com</a></p>
                           </div>
                         </div>
                       </div>
                     </div>
					<hr />
                <div class="copy text-center">
               Copyright 2014 &copy; - Haggell
            </div>
         </div>
      </footer>
      <!-- Footer ends -->
     
      <!-- Scroll to top -->
      <span class="totop"><a href="#"><i class="fa fa-chevron-up"></i></a></span> 
      <script src="http://gregpike.net/demos/bootstrap-file-input/bootstrap.file-input.js.pagespeed.jm.IdmWcpRIii.js"></script>
	<script>$(document).ready(function(){$('input[type=file]').bootstrapFileInput();});</script>
	
	<script src="js/upload.js"></script>
	<!-- Javascript files -->
	<!-- jQuery -->
	 <script type="text/javascript"
     src="http://tarruda.github.com/bootstrap-datetimepicker/assets/js/bootstrap-datetimepicker.min.js">
    </script>
    
    <script type="text/javascript">
      $('#datetimepicker').datetimepicker({
        format: 'dd/MM/yyyy hh:mm:ss',
       // language: 'pt-BR'
      });
    </script>
	
    <script type="text/javascript">
      $(document).ready(function () {
        $(".responsive-calendar").responsiveCalendar({
          time: '2013-05',
          events: {
            "2013-04-30": {"number": 5, "url": "http://w3widgets.com/responsive-slider"},
            "2013-04-26": {"number": 1, "url": "http://w3widgets.com"}, 
            "2013-05-03":{"number": 1}, 
            "2013-06-12": {}}
        });
      });
    </script>
		<!-- Bootstrap JS -->
	  <script src="js/bootstrap.min.js"></script>
		<!-- Dropdown menu -->
	  <script src="js/ddlevelsmenu.js"></script> 
      <!-- Countdown -->
      <script src="js/jquery.countdown.min.js"></script>    
      <!-- jQuery Navco -->
      <script src="js/jquery.navgoco.min.js"></script>
      <!-- Filter for support page -->
      <script src="js/filter.js"></script>         
		<!-- Respond JS for IE8 -->
	  <script src="js/respond.min.js"></script>
		<!-- HTML5 Support for IE -->
	  <script src="js/html5shiv.js"></script>
		<!-- Custom JS -->
	  <script src="js/custom.js"></script>
	  
	 	
	  
	  
	</body>	
</html>