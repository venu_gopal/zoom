<!DOCTYPE html>
<html ng-app="haggel">
	<head>
		<meta charset="utf-8">
		<!-- Title here -->
	    <title>Haggell :: Login</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Your description">
		<meta name="keywords" content="Your,Keywords">
		<meta name="author" content="ResponsiveWebInc">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
      <!-- Animate css -->
      <link href="css/animate.min.css" rel="stylesheet">
      <!-- Dropdown menu -->
      <link href="css/ddlevelsmenu-base.css" rel="stylesheet">
      <link href="css/ddlevelsmenu-topbar.css" rel="stylesheet">
      <!-- Countdown -->
      <link href="css/jquery.countdown.css" rel="stylesheet">     
		<!-- Font awesome CSS -->
		<link href="css/font-awesome.min.css" rel="stylesheet">		
		<!-- Custom CSS -->
		<link href="css/style.css" rel="stylesheet">
    <link href="css/social.css" rel="stylesheet">
		
		<!-- Metro CSS -->
		<link href="css/metro-bootstrap.css" rel="stylesheet">
		<link href="css/metro-bootstrap-responsive.css" rel="stylesheet">
		
		<!-- Favicon -->
		<link rel="shortcut icon" href="#">
	</head>
	
	<body>
	

	
	
	<!-- Login  Modal -->
     <div class="modal fade" id="loginpop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ng-controller="hgLoginCtrl">
       <div class="modal-dialog">
               <form class="form-horizontal" role="form" novalidate="novalidate" name="loginform" ng-submit="loginUser(loginform.$valid)">
                 <div class="modal-content">
                   <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                     <h4 class="modal-title">Login here</h4>
                   </div>
                   <div class="modal-body">
                   
                <div class="form-group" 
                  ng-class="{ 'has-error' : loginform.email.$invalid && !loginform.email.$pristine, 'has-success': loginform.email.$valid && !loginform.email.$pristine}">
                <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                  <input type="email" ng-model="loginRequest.email" name="email" class="form-control" id="inputEmail3" placeholder="Email" required />
                </div>
                </div>
                <div class="form-group" 
                  ng-class="{ 'has-error' : loginform.password.$invalid && !loginform.password.$pristine, 'has-success': loginform.password.$valid && !loginform.password.$pristine}">
                <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                  <input type="password" ng-model="loginRequest.password" class="form-control" id="password" name="password" placeholder="Password" required />
                </div>
                </div>
                <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <div class="checkbox">
                  <label>
                    <input type="checkbox"> Remember me
                  </label>
                  </div>
                </div>
                </div>
              
                    
                   </div>
                   <div class="modal-footer">
                     <button type="button" class="btn btn-default" data-dismiss="modal">Reset</button>
                     <button type="submit" class="btn btn-info">Login</button>
                   </div>
                 </div><!-- /.modal-content -->
              </form>

       </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->
	
	
	
	
	
	

      <!-- Shopping cart Modal -->
     <div class="modal fade" id="shoppingcart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
       <div class="modal-dialog">
         <div class="modal-content">
           <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
             <h4 class="modal-title">Shopping Cart</h4>
           </div>
           <div class="modal-body">
           
            <!-- Items table -->
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Quantity</th>
                  <th>Price</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td><a href="single-item.html">HTC One</a></td>
                  <td>2</td>
                  <td>$250</td>
                </tr>
                <tr>
                  <td><a href="single-item.html">Apple iPhone</a></td>
                  <td>1</td>
                  <td>$502</td>
                </tr>
                <tr>
                  <td><a href="single-item.html">Galaxy Note</a></td>
                  <td>4</td>
                  <td>$1303</td>
                </tr>
                <tr>
                  <th></th>
                  <th>Total</th>
                  <th>$2405</th>
                </tr>
              </tbody>
            </table>
            
           </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal">Continue Shopping</button>
             <button type="button" class="btn btn-info">Checkout</button>
           </div>
         </div><!-- /.modal-content -->
       </div><!-- /.modal-dialog -->
     </div><!-- /.modal -->
           
      <!-- Logo & Navigation starts -->
      
       <!-- Logo & Navigation starts -->
      
      <div class="header">
			<div class="container">
				<div class="row">
					<div class="col-md-2 col-sm-2">
					  <!-- Logo -->
					  <div class="logo">
						 <h1><a href="index.html"><img src="img/logo.png"/></a></h1>
					  </div>
				   </div>
					<div class="col-md-8 col-sm-5">
						<!-- Navigation menu -->
						<div class="navi">
							<div class="mattblackmenu">
								<ul>
									<li><a href="index.html">Home</a></li>
									<li><a href="#">Start Selling</a></li>
									<li><a href="login.html">Signin</a></li>
									<li><a href="register.html">Join</a></li>
									<li><a href="account.html">My profile</a></li>
									<li><a href="#">My Sales</a></li>
									<li><a href="orderhistory.html">My Purchases</a></li>
									<li><a href="contactus.html">Contact us</a></li>
								</ul>
							</div>
						</div>
						<!-- Dropdown NavBar -->
						<div class="navis"></div>                   
					</div>
					<div class="col-md-2 col-sm-5">
						<div class="kart-links pull-right">							
							<a data-toggle="modal" href="#shoppingcart"><i class="fa fa-shopping-cart"></i> 3 Items - $300</a>
						</div>
					</div>
				</div>
			</div>
		</div>
      
      <!-- Logo & Navigation ends -->
     
      <!-- Page content -->
      
      <div class="blocky">
         <div class="container">
            <div class="row">
               <div class="col-md-6">
                  <div class="reg-login-info">
                     <h2>Login to Access Amazing Benefits <span class="color">!!!</span></h2>
                     <img src="img/back1.jpg" alt="" class="img-responsive img-thumbnail" />
                     <p>Duis leo risus, vehicula luctus nunc. Quiue rhoncus, a sodales enim arcu quis turpis. Duis leo risus, condimentum ut posuere ac, vehicula luctus nunc. Quisque rhoncus, a sodales enim arcu quis turpis.</p>
                  </div>
               </div>
               <div class="col-md-6">
			 
			   <div class="text-center col-md-8 col-md-offset-4">
			    <h1 class="text-center login-title">SignUp/SignIn here</h1><br/>
			   <div class="register-login" style="display:none;" ng-controller="hgRegisterCtrl" id="loginbox">
              <form class="form-horizontal" role="form" novalidate="novalidate" name="registerForm" ng-submit="registerUser(registerForm.$valid)">
                <div class="form-group"
                  ng-class="{ 'has-error' : registerForm.username.$invalid && !registerForm.username.$pristine, 'has-success': registerForm.username.$valid && !registerForm.username.$pristine}">
                  <input type="text" class="form-control" ng-model="registerRequest.username" name="username" placeholder="User Name" required>
                </div>
                             
                <div class="form-group"
                  ng-class="{ 'has-error' : registerForm.email.$invalid && !registerForm.email.$pristine, 'has-success': registerForm.email.$valid && !registerForm.email.$pristine}">
                  <input type="email" class="form-control" ng-model="registerRequest.email" name="email" placeholder="Email" required>
                </div>
                <div class="form-group"
                  ng-class="{ 'has-error' : registerForm.password.$invalid && !registerForm.password.$pristine, 'has-success': registerForm.password.$valid && !registerForm.password.$pristine}"> 
                  <input type="password" class="form-control" ng-model="registerRequest.password" name="password" placeholder="Password" required>
                </div> 
               
                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox" name="terms" ng-model="registerRequest.terms" required> I Agree to Terms & Conditions
                        </label>
                      </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                      <button type="submit" class="btn btn-info">Submit</button>
                    </div>
                </div>
                
              </form>   
          </div>
			   
			    <a href="#" class="btn btn-info btn-lg" id="login">Sign Up</a><br/>
			   
			   Or<br/>
			    <a href="#" data-toggle="modal" class="btn btn-link btn-lg" data-target="#loginpop">Sign In</a>
				  
          <div class="social-buttons" ng-controller="hgPassLoginCtrl">
            <a class="btn btn-block btn-social btn-facebook" ng-click="authenticate('facebook')">
              <i class="fa fa-facebook"></i> Sign in with Facebook
            </a>
            <a class="btn btn-block btn-social btn-google-plus" ng-click="authenticate('google');">
              <i class="fa fa-google-plus"></i> Sign in with Google
            </a>
            <a class="btn btn-block btn-social btn-twitter" ng-click="authenticate('twitter');">
              <i class="fa fa-twitter"></i> Sign in with Twitter
            </a>
          </div>
			
				
				
			   </div>
			   
                  
               </div>
            </div>
            <div class="sep-bor"></div>
         </div>
      </div>
     
      
      	
      
      
      
      
     
      
      <!-- Footer starts -->
      <footer>
         <div class="container">
         
               <div class="row">

                        <div class="col-md-4 col-sm-4">
                           <div class="fwidget">
                           
                              <h4>About Us</h4> 
                              <hr />
                              <p>Duis leo risus, vehicula luctus nunc.  Quiue rhoncus, a sodales enim arcu quis turpis. Duis leo risus, condimentum ut posuere ac, vehicula luctus nunc.  Quisque rhoncus, a sodales enim arcu quis turpis.</p>
                              
                              <div class="social">
                                 <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                 <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                 <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                                 <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                                 <a href="#" class="pinterest"><i class="fa fa-pinterest"></i></a>
                              </div>
                           </div>
                       </div>
 
                       <div class="col-md-4 col-sm-4">
                         <div class="fwidget">
                           <h4>Categories</h4>
                           <hr />
                          <ul>
									<li><a href="#">Home</a></li>
									<li><a href="#">Start Selling</a></li>
									<li><a href="login.html">Signin</a></li>
									<li><a href="register.html">Join</a></li>
									<li><a href="account.html">My profile</a></li>
									<li><a href="#">My Sales</a></li>
									<li><a href="orderhistory.html">My Purchases</a></li>
									<li><a href="contactus.html">Contact us</a></li>
								</ul>
                         </div>
                       </div>        

                       

                       <div class="col-md-4 col-sm-4">
                         <div class="fwidget">
                           
                           <h4>Get In Touch</h4>
                           <hr />
                           <div class="address">
                              <p><i class="fa fa-home color contact-icon"></i> #12, Plot No.14, Raj Karmara Street, </p>
                              <p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 5th Stage, Koramangala, Madiwala,</p>
                              <p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Bangalore - 493922, Karananana.</p>
                              <p><i class="fa fa-phone color contact-icon"></i> +94-948-323-5532</p>
                              <p><i class="fa fa-envelope color contact-icon"></i> <a href="mailto:something@gmail.com">info@olsonkart.com</a></p>
                           </div>
                           
                         </div>
                       </div>

                     </div>

         
         
            <hr />
            
            <div class="copy text-center">
               Copyright 2014 &copy; - Haggell
            </div>
         </div>
      </footer>
      <!-- Footer ends -->
      
      <!-- Scroll to top -->
      <span class="totop"><a href="#"><i class="fa fa-chevron-up"></i></a></span> 
      
		<!-- Javascript files -->
		<!-- jQuery -->
		<script src="js/jquery.js"></script>
    <script src="js/angular.min.js"></script>
    <script src="js/satellizer.min.js"></script>
		<!-- Bootstrap JS -->
		<script src="js/bootstrap.min.js"></script>
		<!-- Dropdown menu -->
		<script src="js/ddlevelsmenu.js"></script>  
      <!-- Countdown -->
      <script src="js/jquery.countdown.min.js"></script>    
      <!-- jQuery Navco -->
      <script src="js/jquery.navgoco.min.js"></script>
      <!-- Filter for support page -->
      <script src="js/filter.js"></script>         
		<!-- Respond JS for IE8 -->
		<script src="js/respond.min.js"></script>
		<!-- HTML5 Support for IE -->
		<script src="js/html5shiv.js"></script>
		<!-- Custom JS -->
		<script src="js/custom.js"></script>
	</body>	
</html>