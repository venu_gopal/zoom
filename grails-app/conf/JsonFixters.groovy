import grails.converters.JSON


class JsonFixters {
	def filters = {

		JsonFilters(controller:'(deals|shopper|merchant|adPost|category|profile)', action:'*') {
			after = { model->

				if(model){
					String resp=""
					//log.info "FOund model $model"
					if (model.size() == 1) {
						def nested = model.find { true }.value

						resp= nested as JSON
					} else {
						return true
						//resp= model as JSON
					}

					if(params.callback) {
						resp = params.callback + "(" + resp + ")"
					}

					render (contentType: "application/json", text:resp)
					return false
				}
				else
					return true
			}
		}
	}
}