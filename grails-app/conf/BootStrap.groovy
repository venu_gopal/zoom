
import com.luckydog.dealpost.AdCategory
import com.luckydog.security.Role
import com.luckydog.security.User
import com.luckydog.security.UserRole

class BootStrap {

	def init = { servletContext ->
		log.warn "Boot Strapping .."
		println "Booting strapping data ..."
		if(!Role.findByAuthority('ROLE_USER')) {
			log.info " Added role user"
			def userRole = new Role(authority: 'ROLE_USER')
			userRole.save(flush: false)
		}
		if(!Role.findByAuthority('ROLE_MERCHANT')) {
			def userRole = new Role(authority: 'ROLE_MERCHANT')
			userRole.save(flush: false)
		}
        if(!Role.findByAuthority('ROLE_SHOPPER')) {
			def userRole = new Role(authority: 'ROLE_SHOPPER')
			userRole.save(flush: false)
		}

		if(!Role.findByAuthority('ROLE_ADMIN')) {
			def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: false)
		}


		if(!User.findByUsername("ganesh@alphasigma.com.au")){

			log.warn "Creating first admin user .."
			def aUser = new User(username: "ganesh@alphasigma.com.au", enabled: true, password:"bk123")
			aUser.save(flush:true);
			UserRole ur=	UserRole.create( aUser, Role.findByAuthority('ROLE_ADMIN'), true)
			ur.save(flush:true)
		}

//		if(!AdCategory.findByName("Root")) {
//
//			println "Save ad cat"
//			log.debug "saving ad category.."
//			AdCategory adcat = new AdCategory()
//			adcat.name ="Root"
//			adcat.description="Root Property"
//			adcat.catChildren= new ArrayList();
//			adcat.save(flush:true);
//			log.debug "Saved ad category.."
//		}
		
		
	}
	def destroy = {
	}
}
