class UrlMappings {

	static mappings = {
		"/gimages/${imageId}-${size}.${type}" {
			controller = 'dbContainerImage'
			action = 'index'
		}
		
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/viewImage/$filename" {
			controller = "domainFiles"
			action = "downloadFile"
		}
		
		
		
	
		"/index.gsp"(view:"/index")
		"/"(view:"/index")

      
		"/contactus"(view:"contactus")
		"/register" (view:"register")
		"/cart" (view:"cart")
		"/detail" (view:"/detail")
		
		
		"/paypal/buy/$transactionId?"(controller:"paypal", action:"buy")
		"/paypal/cart/$transactionId"(controller:"paypal", action:"uploadCart")
		"/paypal/notifyPaypal/$buyerId/$transactionId"(controller:"paypal", action:"notifyPaypal")
		"/paypal/success/$buyerId/$transactionId"(controller:"paypal", action:"success")
		"/paypal/cancel/$buyerId/$transactionId"(controller:"paypal", action:"cancel")
        "/category/create"(controller: 'category',action: 'createCategory')
        "/category/delete"(controller: 'category',action: 'deleteCategory')
	//	if (GrailsUtil.isDevelopmentEnv()) {
			"/paypal/test"(view:"/paypal/test")
		//}
		
			
			"500"(view:'/error')
			
	}
	
}
