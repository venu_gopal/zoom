modules = {

	
	stylesheets
	{
		
		resource url:'/css/flatize/css/bootstrap.min.css'
		resource url:'/css/flatize/fonts/font-awesome/css/font-awesome.css'
		resource url:'/css/flatize/owl-carousel/owl.carousel.css'
		resource url:'/css/flatize/owl-carousel/owl.theme.css'
		resource url:'/css/flatize/owl-carousel/owl.transitions.css'
		resource url:'/css/flatize/theme-animate.css'
		resource url:'/css/flatize/theme-elements.css'
		resource url:'/css/flatize/theme-blog.css'
		resource url:'/css/flatize/theme-shop.css'
		resource url:'/css/flatize/theme.css'
		resource url:'/css/flatize/theme-responsive.css'
		resource url:'/css/flatize/style-switcher.css'
		resource url:'/css/flatize/colors/blue/style.css'
		resource url:'/css/flatize/bootstrap-social.css'
	
	}
	
	homepage
	{
		resource url:'/js/flatize/jquery.min.js'
		resource url:'/js/flatize/bootstrap.min.js'
		resource url:'/js/flatize/bootstrap-hover-dropdown.min.js'
		resource url:'/js/flatize/owl-carousel/owl.carousel.js'
		resource url:'/js/flatize/modernizr.custom.js'
		resource url:'/js/flatize/masonry.pkgd.min.js'
		resource url:'/js/flatize/jquery.stellar.js'
		resource url:'/js/flatize/jquery.pricefilter.js'
		resource url:'/js/flatize/jquery.bxslider.min.js'
		resource url:'/js/flatize/mediaelement-and-player.js'
		resource url:'/js/flatize/waypoints.min.js'
		resource url:'/js/flatize/theme.plugins.js'
		resource url:'/js/flatize/theme.js'
		
	}
	
	
	


	
	loggedin
	{
		resource url:'/js/flatize/jquery.min.js'
		resource url:'/js/flatize/bootstrap.min.js'
	}
	
	anonymous
	{
		resource url:'/js/flatize/jquery.min.js'
		resource url:'/js/flatize/bootstrap.min.js'
		resource url:'/js/flatize/bootstrap-hover-dropdown.min.js'
		resource url:'/js/flatize/owl-carousel/owl.carousel.js'
		resource url:'/js/flatize/modernizr.custom.js'
		resource url:'/js/flatize/masonry.pkgd.min.js'
		resource url:'/js/flatize/jquery.stellar.js'
		resource url:'/js/flatize/jquery.pricefilter.js'
		resource url:'/js/flatize/jquery.bxslider.min.js'
		resource url:'/js/flatize/mediaelement-and-player.js'
		resource url:'/js/flatize/waypoints.min.js'
		resource url:'/js/flatize/theme.plugins.js'
		resource url:'/js/flatize/theme.js'
			
		
	}
	
	cart
	{
		
	}
	
	
	timeline
	{
		dependsOn "metro"
		resource url:'/js/flatize/timeline.js'
		resource url:'/js/flatize/profile.js'
	}
	
	metro
	{
		resource url:'/css/flatize/metro/style.css'
		//resource url:'/css/flatize/metro/metro-bootstrap.css'
		//resource url:'/css/flatize/metro/metro-bootstrap-responsive.css'
	}
	
}