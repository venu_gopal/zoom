grails.servlet.version = "3.0" // Change depending on target container compliance (2.5 or 3.0)
//grails.project.dependency.resolver = "ivy" // or ivy
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"

grails.project.target.level = 1.7
grails.project.source.level = 1.7
grails.project.war.file = "target/zoom.war"
grails.project.dependency.resolver = "maven"
// uncomment (and adjust settings) to fork the JVM to isolate classpaths
grails.project.fork = [
	run: [maxMemory:1024, minMemory:64, debug:false, maxPerm:256]
]

grails.project.dependency.resolution = {
	// inherit Grails' default dependencies
	inherits("global") {
		//        // specify dependency exclusions here; for example, uncomment this to disable ehcache:
		//  excludes 'ehcache',"h2" //,"hibernate-jpa-2.0-api","spring-jdbc"
		excludes "spring-test","javaee-web-api"
	}
	log "info" // log level of Ivy resolver, either 'error', 'warn', 'info', 'debug' or 'verbose'
	checksums false // Whether to verify checksums on resolve
	legacyResolve false // whether to do a secondary resolve on plugin installation, not advised and here for backwards compatibility

	repositories {
		inherits true // Whether to inherit repository definitions from plugins
		grailsPlugins()
		grailsHome()
		grailsCentral()
		mavenLocal()
		mavenCentral()
		mavenRepo "http://guice-maven.googlecode.com/svn/trunk/"

		// uncomment these (or add new ones) to enable remote dependency resolution from public Maven repositories
		mavenRepo 'http://repo.spring.io/milestone'
		mavenRepo "http://snapshots.repository.codehaus.org"
		mavenRepo "http://repository.codehaus.org"
		mavenRepo "http://download.java.net/maven/2/"
		//mavenRepo "http://repository.jboss.com/maven2/"
	}

	dependencies {
		compile "org.mongodb:mongo-java-driver:2.12.3"
		//	runtime "com.gmongo:gmongo:1.3"
		//compile "com.paypal.sdk:rest-api-sdk:0.5.2"
		compile "net.sf.ehcache:ehcache-core:2.4.8"
		compile 'org.grails:grails-datastore-gorm:3.1.2.RELEASE'
		compile 'org.grails:grails-datastore-gorm-plugin-support:3.1.2.RELEASE'
		compile 'org.grails:grails-datastore-core:3.1.2.RELEASE'
		test 'org.grails:grails-datastore-simple:3.1.2.RELEASE'
	}

	plugins {
		build ":tomcat:7.0.55"

		excludes = "hibernate, domain"
		//runtime ":jquery:1.8.0"
		//	compile ":jquery-ui:1.10.3"
		//runtime ":modernizr:2.5.3"
		runtime ":resources:1.2.8"
		//		compile ':mongodb:1.0.0.GA'
		//compile ":famfamfam:1.0.1"
		compile ":webxml:1.4.1"
		// compile ":mongodb:1.3.3"
		compile (":mongodb:3.0.2"){ excludes "grails-datastore-gorm","grails-datastore-gorm-plugin-support","grails-datastore-core"}

		// Uncomment these (or add new ones) to enable additional resources capabilities
		//runtime ":zipped-resources:1.0"
		//runtime ":cached-resources:1.0"
		//runtime ":yui-minify-resources:0.1.4"
		//compile ":uploadr:0.5.11"
		//    build ":tomcat:$grailsVersion"
		// runtime ":database-migration:1.1"
		// compile ':cache:1.0.0'
		compile ":spring-security-core:2.0-RC4"
		//	compile ":spring-security-ui:0.2"
		//compile ":searchable:0.6.4"
		//compile ":spring-security-facebook:0.10"
		//compile ":spring-security-openid:1.0.4"
		//compile ":quartz2:0.2.3"
		//	compile ":export:1.5"
		//compile ":qrcode:0.3"
		//compile ":google-chart:0.5.2"
		compile ':scaffolding:2.0.3'
		compile ":paypal:0.6.8"

	}
}
