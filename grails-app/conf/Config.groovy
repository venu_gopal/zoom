// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [
	all:           '*/*',
	atom:          'application/atom+xml',
	css:           'text/css',
	csv:           'text/csv',
	form:          'application/x-www-form-urlencoded',
	html:          [
		'text/html',
		'application/xhtml+xml'
	],
	js:            'text/javascript',
	json:          [
		'application/json',
		'text/json'
	],
	multipartForm: 'multipart/form-data',
	rss:           'application/rss+xml',
	text:          'text/plain',
	xml:           [
		'text/xml',
		'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000
grails.gorm.failOnError=true
// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = [
	'/images/*',
	'/css/*',
	'/js/*',
	'/plugins/*'
]
grails.plugin.quartz2.autoStartup = false
grails.config.locations = ["file:QuartzConfig.groovy"]
// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false
grails.gorm.failOnError=true
// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']


grails.plugin.springsecurity.password.algorithm='bcrypt'

environments {
	development {
		grails.logging.jul.usebridge = true
	}
	production {
		grails.logging.jul.usebridge = false
		grails.serverURL = "http://www.haggell.com"
	}
}

environments {
	production {
	   grails.paypal.server = "https://www.paypal.com/cgi-bin/webscr"
	   grails.paypal.email = "example@business.com"
//	   grails.serverURL = "http://127.0.0.1:8080/zoom"


    }
	development {
	   grails.paypal.server = "https://www.sandbox.paypal.com/cgi-bin/webscr"
	   grails.paypal.email = "finance@alphasigma.com.au"
        grails.serverURL = "http://127.0.0.1:8080/zoom"
	  // grails.serverURL = "http://812.99.101.131"
        google.apiKey='517423181965-qjfailcuourro0hd7ri6etamcp8ha4j2.apps.googleusercontent.com'
        google.apiSecret='-J5CfOjWFFOPMSSr_Cns7Hy2'

        facebook.apiKey='413897408738193'
        facebook.apiSecret='69ced2314a77da76816608448e0127c0'
	}
  }

// log4j configuration
log4j = {
	

	info  'org.codehaus.groovy.grails.web.servlet',        // controllers
			'org.codehaus.groovy.grails.web.pages',          // GSP
			'org.codehaus.groovy.grails.web.sitemesh',       // layouts
			'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
			'org.codehaus.groovy.grails.web.mapping',        // URL mapping
			'org.codehaus.groovy.grails.commons',            // core / classloading
			'org.codehaus.groovy.grails.plugins',            // plugins
			'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
			'org.springframework',
			'org.mortbay.log'

	debug 'com.luckydog', "grails.app"
}


mongodb {
	host = "127.0.0.1"
	port = 27017
	username = "gvenez"
	password = "bkaro123"
	databaseName = "karmawar"
}

grails {
	mail {
	  host = "smtp.gmail.com"
	  port = 465
	  username = "test@gmail.com"
	 
	  props = ["mail.smtp.auth":"true",
			   "mail.smtp.socketFactory.port":"465",
			   "mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactory",
			   "mail.smtp.socketFactory.fallback":"false"]
	}
 }


// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'com.luckydog.security.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'com.luckydog.security.UserRole'
grails.plugin.springsecurity.authority.className = 'com.luckydog.security.Role'
grails.plugin.springsecurity.password.algorithm='SHA-512'
grails.plugin.springsecurity.roleHierarchy ="'ROLE_SUPERADMIN>ROLE_ADMIN>ROLE_MERCHANT>ROLE_USER'"

grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	'/**':               ['permitAll'],
	'/shopper/register':               ['permitAll'],
	'/merchant/register':               ['permitAll'],
	'/index':          ['permitAll'],
	'/index.gsp':      ['permitAll'],
	'/assets/**':      ['permitAll'],
	'/**/js/**':       ['permitAll'],
	'/**/css/**':      ['permitAll'],
	'/**/images/**':   ['permitAll'],
	'/**/favicon.ico': ['permitAll']
 ]

// Uncomment and edit the following lines to start using Grails encoding & escaping improvements

/* remove this line 
// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside null
                scriptlet = 'none' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        filteringCodecForContentType {
            //'text/html' = 'html'
        }
    }
}
remove this line */
