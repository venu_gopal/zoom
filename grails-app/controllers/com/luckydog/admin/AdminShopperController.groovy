package com.luckydog.admin

import grails.converters.JSON

import com.luckydog.shopper.Shopper


class AdminShopperController {


    def listShoppers() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def shopperInstanceList = Shopper.list(params)
        render shopperInstanceList as JSON
    }


    def suspendUser() {
        Shopper shopperInstance = Shopper.get(params.id)
        shopperInstance.isActive = false
        shopperInstance.save()
        render shopperInstance as JSON
    }

    def unsuspendUser() {
        Shopper shopperInstance = Shopper.get(params.id)
        shopperInstance.isActive = true
        shopperInstance.save()
        render shopperInstance as JSON
    }


}
