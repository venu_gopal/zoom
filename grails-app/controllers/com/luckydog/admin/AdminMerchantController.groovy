package com.luckydog.admin

import grails.converters.JSON

import com.luckydog.merchant.Merchant


class AdminMerchantController {

    def listMerchants() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        def merchantInstanceList = Merchant.list(params)
        render merchantInstanceList as JSON
    }

    def deleteMerchant() {
        def merchantInstance = Merchant.get(params.id)
        merchantInstance.delete(flush: true)
        true
    }

    def suspendMerchant() {
        Merchant merchantInstance = Merchant.get(params.id)
        merchantInstance.isActive = false
        merchantInstance.save()
        render merchantInstance as JSON
    }

    def unsuspendMerchant() {
        Merchant merchantInstance = Merchant.get(params.id)
        merchantInstance.isActive = true
        merchantInstance.save()
        render merchantInstance as JSON
    }
    /**
     * More usecases
     */
}
