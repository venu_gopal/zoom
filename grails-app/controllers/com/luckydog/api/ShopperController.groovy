package com.luckydog.api

import grails.converters.JSON

import org.springframework.security.access.annotation.Secured

import com.luckydog.security.Role
import com.luckydog.security.User
import com.luckydog.security.UserRole
import com.luckydog.shopper.PurchaseHistory
import com.luckydog.shopper.Shopper
import com.luckydog.utils.ErrorObject
import com.luckydog.utils.social.user.ShopperCO

class ShopperController {
	def springSecurityService;


	def signUp () {
		render(view: "/shopper/shopperRegistration");
	}
	
	def profile()
	{
		render(view:"/profile/index")
	}
	
	def cart()
	
	{
		render(view:"/cart")
	}

	def register = { ShopperCO shopperCO ->
		Shopper shopperInstance
		log.debug("+++++++++++++++++Under shopper params+++++" + params)
		log.info "Registering Shopper .."


		String email = params.email
		if(!email || ! params.password) {

			ErrorObject err = new ErrorObject()
			err.errorNumber = 1301
			err.errorDescription = "Please enter a valid email address and password for registering the merchant"
			log.error("-----------------------invalid params")
			render err as JSON
			return;
		}
		//check if user already exists
		if (User.findByUsername(email)) {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 1302
			err.errorDescription = "Email is already registered. <br> Please enter a different email address or login to continue"
			log.error("------------------------email id exist")
			render err as JSON
			return;

		}

		/*bindData(shopperInstance, params, [include: ['name','password']])
		 shopperInstance.username=params.email;*/

		if (shopperCO.validate()) {
			shopperInstance = new Shopper()
			shopperInstance.username = shopperCO.email;
			shopperInstance.enabled = true
			shopperInstance.name = shopperCO.name;
			shopperInstance.password = springSecurityService.encodePassword(shopperCO.password)
			shopperInstance.save(flush: true)
			def shopperRole = Role.findByAuthority('ROLE_SHOPPER')

			UserRole ur = UserRole.create(shopperInstance, shopperRole, true)
			ur.save(flush: true)
			log.debug("Shopper saved successfully.......................")
			render shopperInstance as JSON
		} else {

			log.error("<<<<<<<<<<>>>>>>>>>>>Existed Errors" + shopperInstance.errors)
			ErrorObject err = new ErrorObject()
			err.errorNumber = 1304
			err.errorDescription = "No such AdPost"
			render err as JSON
		}


	}

	@Secured(['ROLE_SHOPPER', 'ROLE_ADMIN'])
	def purchaseHistory () {

		User user = springSecurityService.currentUser as User
		println(user.username + "----------" + user.id)
		List<PurchaseHistory> purchasePost = PurchaseHistory.findAllByBuyerId(user.id)
		//List pList=user.boughtItems;
		//println("+++++++++++++++++++++++++++"+pList)
		render purchasePost as JSON
		//return [model: user.boughtItems]

	}


}
