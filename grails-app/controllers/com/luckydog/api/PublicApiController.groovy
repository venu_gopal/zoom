package com.luckydog.api



class PublicApiController {

    def gridfsService
    def springSecurityService
    def mongo
    //static def listInitSize;

    /*def checkUniqueEmail = {
        println '-------checkUniqueEmail-----------------------' + params
        Map resultMap = ['resultData': 'false']
        if (User.countByUsername(params.email)) {
            resultMap['resultData'] = 'true'
        }
        println "============resultMap=============" + resultMap
        render resultMap as JSON
    }

    def getDealsJSON = {
        println '---------getDealsJSON------------Params----------------------' + params
        List<AdPost> adPostList = AdPost.listOrderById();
        List<AdPostVO> adPostVOList = new ArrayList<AdPostVO>();
        adPostList.each {
            adPostVOList.add(new AdPostVO(it));
        }
        render adPostVOList as JSON
    }

    def getMerchantsDealJSON = {
        Merchant merchant = springSecurityService.currentUser as Merchant;
        List<AdPost> adPostList = AdPost.findAllByMerchant(merchant);
        List<AdPostVO> adPostVOList = new ArrayList<AdPostVO>();
        adPostList.each {
            adPostVOList.add(new AdPostVO(it));
        }
        println('+++++++++++++++++++total size of current user posts.........'+adPostVOList.size());
        render adPostVOList as JSON;
    }

    def getRecentMerchantDealJSON = {
        Merchant merchant = springSecurityService.currentUser as Merchant;
        AdPost adPost = AdPost.findByMerchant(merchant, [order: 'desc', sort: 'dateCreated']);
        AdPostVO adPostVO = new AdPostVO(adPost);
        println '--------getRecentMerchantDealJSON adPost-------------' + (adPost as JSON)
        render adPostVO as JSON;
    }

//    *********************************************** API Actions ****************************************

    def editMerchantDealJSON() {
        println("Under------------------editMerchantDealJSON-------------------" + params.id);
        AdPost adPostEdit = AdPost.get(params.id);
        AdPostVO adPostVO = new AdPostVO(adPostEdit);
        def adPostVOJSON = adPostVO as JSON;
        println("this is json of edit deal=====================" + adPostVOJSON)
        render adPostVOJSON;
    }


    def buyDeal() {
        println('+++++++++++++buyDeal+++++params+++++++++' + params)
        Long id = (params.id) as Long
        PurchaseHistory purchaseHistory = new PurchaseHistory();
        println("current user id=" + springSecurityService.currentUser.id)
        purchaseHistory.buyerId = springSecurityService.currentUser.id
        purchaseHistory.itemId = id;
        purchaseHistory.dateCreated = new Date()
        purchaseHistory.dateUpdated = new Date()
        println('purchase history object created......')
        purchaseHistory.status = 1;
        purchaseHistory.save(flush: true);
        println("purchase history object save successfully");
        render purchaseHistory as JSON
    }

    def getBuyersList(Long id) {
        println('++++++++Under Show Buyers++++++++++' + params)
        List<PurchaseHistory> purchaseHistoryList = PurchaseHistory.findAllByItemId(id)
        List<PurchaseHistoryVO> historyVOList = []
        purchaseHistoryList.each {
            historyVOList.add(new PurchaseHistoryVO(it));
        }
        println("++++++++++++purchaseHistoryVOList++++++as+++JSON " + (historyVOList as JSON))
        render(historyVOList as JSON)
    }

    def getPurchaseDealList() {
        User currentUser = springSecurityService.currentUser as User;
        List<PurchaseHistory> purchaseHistoryList = PurchaseHistory.findAllByBuyerId(currentUser.id,[order:'desc' , sort:'id']);
        List<AdPostVO> adPostVOList = []
        purchaseHistoryList.each {
            adPostVOList.add(new AdPostVO(it.adPost));
        }
        println("AdPostVolist JSON is " + (adPostVOList as JSON))
        render(adPostVOList as JSON);
    }
    def getCurrentPurchasePosts(){
        User currentUser = springSecurityService.currentUser as User;
        PurchaseHistory purchaseHistoryList = PurchaseHistory.findByBuyerId(currentUser.id,[order:'desc' , sort:'id']);
        AdPostVO postVO=new AdPostVO(purchaseHistoryList.adPost);
        println("---------AdPostVo-----------"+postVO);
        render(postVO as JSON);
    }*/
}
