package com.luckydog.api

import org.springframework.security.access.annotation.Secured
import org.springframework.web.servlet.ModelAndView

import com.luckydog.dealpost.AdPost
import com.luckydog.shopper.Shopper
import com.luckydog.utils.ErrorObject


//FIXME this to make the correct profile
class ProfileController {

	
  //  @Secured(['IS_AUTHENTICATED_FULLY'])
    public def index() {
//        String username = getPrincipal().username
//        if (request.isUserInRole("ROLE_USER")) {
//
//        } else if (request.isUserInRole("ROLE_MERCHANT")) {
//
//        } else if (request.isUserInRole("ROLE_ADMIN")) {
//
//        }

    }

    @Secured(['IS_AUTHENTICATED_FULLY'])
    public def showProfile() {
        String username = getPrincipal().username
        if (request.isUserInRole("ROLE_USER")) {
            Shopper shopperInstance = Shopper.findByEmail(username)
            //render(view: "")
            List postItems
            int i = 0
            for (AdPost post in shopperInstance.boughtItems) {
                if (i++ > 5) break
                postItems << post
            }
            return new ModelAndView("/profile/shopperProfile", [shopper: shopperInstance, posts: postItems])
        }

        if (request.isUserInRole("ROLE_MERCHANT")) {
            render(view: "merchantProfile")
        }


    }

    @Secured(['IS_AUTHENTICATED_FULLY'])
    def updateProfile() {
        String username = getPrincipal().username
        Shopper shopperInstance = Shopper.findByEmail(username)

        shopperInstance.properties = params

        if (!shopperInstance.save(flush: true)) {
            ErrorObject err = new ErrorObject();
            err.errorDescription = "Could not Save Shopper"
            err.errorNumber = 501
            return [model: err]
        }

        return [model: shopperInstance]
    }

    @Secured(['IS_AUTHENTICATED_FULLY'])
    def deleteProfile() {
        String username = getPrincipal().username
        Shopper shopperInstance = Shopper.findByEmail(username)
        shopperInstance.delete(flush: true)
    }


}
