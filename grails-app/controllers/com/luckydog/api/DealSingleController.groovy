package com.luckydog.api

import grails.converters.JSON

import java.text.SimpleDateFormat

import org.springframework.security.access.annotation.Secured

import com.luckydog.AdPostVO
import com.luckydog.PurchaseHistoryVO
import com.luckydog.dealpost.AdCategory
import com.luckydog.dealpost.AdPost
import com.luckydog.merchant.Merchant
import com.luckydog.security.User
import com.luckydog.shopper.PurchaseHistory
import com.luckydog.utils.ErrorObject
import com.luckydog.utils.ImageTool
import com.luckydog.utils.Karma

class DealSingleController {

	def gridfsService
	def mongo
	def springSecurityService


	
	def createDeal () {
		render(view: "/manageDeals/createDeal")
	}

	def showdetails() {
		render (view:"/detail")
		
	}
	def showDeal() {
		def Id = params.adid as long
		def post = AdPost.get(Id)
		AdPostVO adPostVO = new AdPostVO(post);
		//return [model:post]
		render adPostVO as JSON;
	}


	def showRating() {
		Long adid = params.adid as Long
		def db = mongo.getDB(grailsApplication.config.mongodb.databaseName)
		def xoutput = db.karma.aggregate(['$match': [adPostId: adid, karmaType: 2]], ['$group': [_id: [krm: '$karmaType'], average: [$avg: '$krating']]])
		println("ok " + (xoutput as JSON))
		render(xoutput.results()) as JSON
		//return [model: [avg: xoutput.results()] ]

	}

	//TODO: Fix my personal ratings
	@Secured([
		'IS_AUTHENTICATED_REMEMBERED'
	])
	def showPersonalRating() {
		log.info " --> " + getPrincipal()
		String username = getPrincipal().username
		log.info " username is $username"
		Long adid = params.adid.toLong()
		Karma karma = Karma.findByAdPostIdAndShopperId(adid, username)
		println(karma)
		Map results = [:]
		if (karma) {
			results["rating"] = karma.krating
		} else {
			results["rating"] = 0
		}
		println(results)
		render results as JSON
		//return [model: [res: results]]
	}


	@Secured([
		'IS_AUTHENTICATED_REMEMBERED'
	])
	def rateDeal() {
		String username = getPrincipal()?.username
		println("+++++++++++" + username)
		if (!username) {
			return [model: [res: "-401"]]
		}
		Long id = params.adid.toLong()
		Double ratevalue = params.value.toDouble()
		AdPost post = AdPost.get(id)
		Karma karma = Karma.findByAdPostIdAndShopperId(id, username)
		if (null == karma) {

			karma = new Karma();
			karma.shopperId = username
			karma.adPostId = id
			karma.karmaType = 2
		} else {
			//karma.karmaType=1
		}
		karma.krating = ratevalue
		karma.save(flush: true)
		render karma as JSON;
		//return [model: [res: "+OK"]]
	}


	@Secured(['ROLE_MERCHANT'])
	def saveDeal() {
		println("_++++++++++++++++++++______________++++++++++++++++________")
		log.info "fetched $params"
		//		String username = getPrincipal()?.username
		//		def merchant = Merchant.findByEmail(username)
		def merchant = springSecurityService.currentUser as Merchant
		if (!merchant) {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such merchant"
			return [model: err]
		}

		def adPostInstance = new AdPost()
		// binds request parameters to a target object
		bindData(adPostInstance, params, [exclude: [
				'adCategories',
				'comments',
				"endDate",
				"startDate"
			]])

		if (params.unlimited) {
			adPostInstance.quantity = -1
		}
		adPostInstance.merchant = merchant

		SimpleDateFormat format = new SimpleDateFormat("mm/dd/YYYY hh:mm");
		//adPostInstance.endDate=format.parse(params.endDate)
		//adPostInstance.startDate=format.parse(params.startDate)
		adPostInstance.endDate = new Date(params.endDate)
		adPostInstance.startDate = new Date(params.startDate)
		adPostInstance.location = [
			params.latid.toDouble(),
			params.lonid.toDouble()
		]

		adPostInstance.adCategories = []
		if (params.adCategories) {


			List catlist = new ArrayList();

			List adCategories = params.list('adCategories')
			adCategories.each { cat ->
				def adCategoryInstance = AdCategory.get(params.adCategories)
				if (adCategoryInstance)
					catlist.add(adCategoryInstance)
			}

			adPostInstance.adCategories = catlist
		}


		def file = request.getFile('logo')
		//move this to background service!

		if (file) {
			log.info "file size is ${file.size}"

			if (file.size == 0 || file.size > 3145728) {
				//adPostInstance.isActive=false
				ErrorObject err = new ErrorObject()
				err.errorNumber = 301
				err.errorDescription = "Image is either empty or exceeds size"
				return [model: err]
			}
		}


		if (!adPostInstance.save(flush: true)) {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 303
			err.errorDescription = "Could not create ad. Some error"
			adPostInstance.errors.allErrors.each {
				err.errors.put(adPostInstance.errors.fieldError.field, it)
			}
			return [model: err]
		}

		//move this to the background?
		if (file) {
			def imageTool = new ImageTool()
			imageTool.load(file.getBytes())
			imageTool.thumbnailSpecial(960, 720, 1, 1)
			byte[] arr = imageTool.getBytes("JPEG")
			gridfsService.saveFile(arr, file.contentType, adPostInstance.id.toString(), "back960.jpg", 960, 720)

			//imageTool.load(file.getBytes())
			//imageTool.thumbnailSpecial(640, 480, 1, 1)
			//	arr = imageTool.getBytes("JPEG")
			//	gridfsService.saveFile(arr, file.contentType,adPostInstance.id.toString(),"back640.jpg", 640, 480)

			imageTool.load(file.getBytes())
			imageTool.thumbnailSpecial(460, 320, 1, 1)
			arr = imageTool.getBytes("JPEG")
			gridfsService.saveFile(arr, file.contentType, adPostInstance.id.toString(), "back460.jpg", 460, 320)

			//imageTool.load(file.getBytes())
			//imageTool.thumbnailSpecial(250, 250, 1, 1)
			//arr = imageTool.getBytes("JPEG")
			//gridfsService.saveFile(arr, file.contentType,adPostInstance.id.toString(),"back250.jpg",250,250)
		}
		println("===========================Saved Deal Successfully=============== " + adPostInstance.id + " ============" + adPostInstance.title)
		//		return [model:adPostInstance]
		render adPostInstance as JSON
	}

	//**********************public APi action's*****************************
	def checkUniqueEmail () {
		println '-------checkUniqueEmail-----------------------' + params
		Map resultMap = ['resultData': 'false']
		if (User.countByUsername(params.email)) {
			resultMap['resultData'] = 'true'
		}
		println "============resultMap=============" + resultMap
		render resultMap as JSON
	}

	def getDealsJSON(){
		println '---------getDealsJSON------------Params----------------------' + params
		List<AdPost> adPostList = AdPost.listOrderById();
		List<AdPostVO> adPostVOList = new ArrayList<AdPostVO>();
		adPostList.each {
			adPostVOList.add(new AdPostVO(it));
		}
		render adPostVOList as JSON
	}

	def getMerchantsDealJSON () {
		Merchant merchant = springSecurityService.currentUser as Merchant;
		List<AdPost> adPostList = AdPost.findAllByMerchant(merchant);
		List<AdPostVO> adPostVOList = new ArrayList<AdPostVO>();
		adPostList.each {
			adPostVOList.add(new AdPostVO(it));
		}
		println('+++++++++++++++++++total size of current user posts.........'+adPostVOList.size());
		render adPostVOList as JSON;
	}

	def getRecentMerchantDealJSON () {
		Merchant merchant = springSecurityService.currentUser as Merchant;
		AdPost adPost = AdPost.findByMerchant(merchant, [order: 'desc', sort: 'dateCreated']);
		AdPostVO adPostVO = new AdPostVO(adPost);
		println '--------getRecentMerchantDealJSON adPost-------------' + (adPost as JSON)
		render adPostVO as JSON;
	}

	//    *********************************************** API Actions ****************************************

	def editMerchantDealJSON() {
		println("Under------------------editMerchantDealJSON-------------------" + params.id);
		AdPost adPostEdit = AdPost.get(params.id);
		AdPostVO adPostVO = new AdPostVO(adPostEdit);
		def adPostVOJSON = adPostVO as JSON;
		println("this is json of edit deal=====================" + adPostVOJSON)
		render adPostVOJSON;
	}


	def buyDeal() {
		println('+++++++++++++buyDeal+++++params+++++++++' + params)
		Long id = (params.id) as Long
		PurchaseHistory purchaseHistory = new PurchaseHistory();
		println("current user id=" + springSecurityService.currentUser.id)
		purchaseHistory.buyerId = springSecurityService.currentUser.id
		purchaseHistory.itemId = id;
		purchaseHistory.dateCreated = new Date()
		purchaseHistory.dateUpdated = new Date()
		println('purchase history object created......')
		purchaseHistory.status = 1;
		purchaseHistory.save(flush: true);
		println("purchase history object save successfully");
		render purchaseHistory as JSON
	}

	def getBuyersList(Long id) {
		println('++++++++Under Show Buyers++++++++++' + params)
		List<PurchaseHistory> purchaseHistoryList = PurchaseHistory.findAllByItemId(id)
		List<PurchaseHistoryVO> historyVOList = []
		purchaseHistoryList.each {
			historyVOList.add(new PurchaseHistoryVO(it));
		}
		println("++++++++++++purchaseHistoryVOList++++++as+++JSON " + (historyVOList as JSON))
		render(historyVOList as JSON)
	}

	def getPurchaseDealList() {
		User currentUser = springSecurityService.currentUser as User;
		List<PurchaseHistory> purchaseHistoryList = PurchaseHistory.findAllByBuyerId(currentUser.id,[order:'desc' , sort:'id']);
		List<AdPostVO> adPostVOList = []
		purchaseHistoryList.each {
			adPostVOList.add(new AdPostVO(it.adPost));
		}
		println("AdPostVolist JSON is " + (adPostVOList as JSON))
		render(adPostVOList as JSON);
	}
	def getCurrentPurchasePosts(){
		User currentUser = springSecurityService.currentUser as User;
		PurchaseHistory purchaseHistoryList = PurchaseHistory.findByBuyerId(currentUser.id,[order:'desc' , sort:'id']);
		AdPostVO postVO=new AdPostVO(purchaseHistoryList.adPost);
		println("---------AdPostVo-----------"+postVO);
		render(postVO as JSON);
	}

}
