package com.luckydog.api

import grails.converters.JSON

import org.springframework.security.access.annotation.Secured

import com.luckydog.dealpost.AdPost
import com.luckydog.merchant.Merchant
import com.luckydog.security.Role
import com.luckydog.security.User
import com.luckydog.security.UserRole
import com.luckydog.utils.ErrorObject
import com.luckydog.utils.ImageTool
import com.luckydog.utils.SuccessObject

class MerchantController {
	def gridfsService
	def springSecurityService


	def signUp = { render(view: 'registerNew') }


	def oldregister() {
		println '---------------------------------' + params

		String email = params.username
		if ( !email || User.findByUsername(email)) {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 301
			err.errorDescription = "Email is already registered. <br> Please enter a different email address or login to continue"
			render err as JSON
			return ;
		}

		Merchant merchantInstance = new Merchant(params)
		//store address

		merchantInstance.latitude = params.latid.toDouble()
		merchantInstance.longitude = params.lonid.toDouble()
		merchantInstance.password = springSecurityService.encodePassword(params.password)
		merchantInstance.enabled = true

		if (!merchantInstance.save(flush: true, failOnError: true)) {
			render  merchantInstance as JSON
		}

		Role userRole = Role.findByAuthority('ROLE_MERCHANT')
		UserRole ur = UserRole.create(merchantInstance, userRole, true)
		ur.save(flush: true)


		if(params.logo)
		{
			def file = request.getFile('logo')
			if (file) {
				ImageTool imageTool = new ImageTool()
				imageTool.load(file.getBytes())
				imageTool.thumbnail(120)
				byte[] arr = imageTool.getBytes("JPEG")
				gridfsService.saveFile(arr, file.contentType, merchantInstance.id.toString(), "merchant.jpg", 120, 120)
			}

		}

		render merchantInstance as JSON
	}


	@Secured(['ROLE_ADMIN'])
	def deleteProfile() {
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)
		if (merchant) {
			merchant.delete(flush: true)
			[model: new SuccessObject(code: 200, message: "Success Delete")]
		} else {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such AdPost"
			return [model: err]
		}
	}

	@Secured(['ROLE_MERCHANT'])
	def deactivateProfile() {
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)
		if (merchant) {
			merchant.enabled = false
			merchant.save(flush: true)
			[model: new SuccessObject(code: 200, message: "Success Deactivate")]
		} else {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such AdPost"
			return [model: err]
		}
	}

	def showProfile() {
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)
		if (merchant) {
			return [model: merchant]
		} else {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such Merchant"
			return [model: err]
		}
	}

	@Secured(['ROLE_MERCHANT'])
	def updateProfile() {
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)
		//BUGFIX put in excludes
		merchant.properties = params
		if (!merchant.save(flush: true)) {
			ErrorObject err = new ErrorObject();
			err.errorDescription = "Could not Save Merchant"
			err.errorNumber = 501
			return [model: err]
		}
		return [model: merchant]
	}


	@Secured(['ROLE_MERCHANT'])
	def showMyItems() {
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)
		if (!merchant) {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such merchant"
			return [model: err]
		}
		println "Found merchant ${merchant.adposts}"
		return [model: merchant.adposts]
	}


	@Secured(['ROLE_MERCHANT'])
	def deletead() {
		long adid = params.adid as long
		log.info "Deleting AdPost .. $adid"
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)

		if (!merchant) {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such merchant"
			return [model: err]
		}

		AdPost post = merchant.adposts.find { it.id == adid }
		if (post) {
			post.delete(flush: true)
			return [model: ["OK"]]
		} else {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such AdPost"
			return [model: err]
		}
	}

	@Secured(['ROLE_MERCHANT'])
	def deactivatead() {
		Long adid = params.long('adid')
		log.info ".... Deactivating AdPost .... $adid"
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)

		if (!merchant) {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such merchant"
			return [model: err]
		}
		AdPost post = merchant.adposts.find { it.id == adid }
		if (post) {
			post.isActive = false
			post.save(flush: true)
			return [model: ["OK"]]
		} else {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such AdPost"
			return [model: err]
		}
	}

	@Secured(['ROLE_MERCHANT'])
	def activatead() {
		println("Under activated.............")
		long adid = params.adid as long
		log.info "Activating AdPost .. $adid"
		User user = springSecurityService.currentUser as User
		Merchant merchant = Merchant.findByUsername(user.username)

		if (!merchant) {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such merchant"
			render err as JSON
			//return [model: err]
		}

		AdPost post = merchant.adposts.find { it.id == adid }
		if (post) {
			post.isActive = true
			post.save(flush: true)
			//render "OK+" as JSON;
			return [model: ["OK"]]
		} else {
			ErrorObject err = new ErrorObject()
			err.errorNumber = 302
			err.errorDescription = "No such AdPost"
			render err as JSON
			//return [model: err]
		}
	}
}
