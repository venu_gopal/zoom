package com.luckydog.location

import org.springframework.web.servlet.ModelAndView

class LocationController {

    def index() {
        def locations = [:]

        return new ModelAndView("/location/city", [locations: locations])
    }

    def getCountries() {
        //return the country list
    }

    def getCities() {
        def countryName = params.country
        //get the list of all cities that belong to this country
    }

    def getLocationFromIp() {
        String ipaddress = params.ip

    }
}
