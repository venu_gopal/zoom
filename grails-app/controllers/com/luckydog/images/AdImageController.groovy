package com.luckydog.images

import com.luckydog.merchant.Merchant
import com.mongodb.gridfs.GridFS
import com.mongodb.gridfs.GridFSDBFile
import org.springframework.dao.DataIntegrityViolationException

import com.luckydog.utils.ImageTool
import grails.converters.JSON

class AdImageController {
    def imageUploadService
    def gridfsService

    // static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        def fname = params.name

        def fileForOutput = gridfsService.retrieveFile(fname)
        if (!fileForOutput) return
        response.outputStream << fileForOutput.getInputStream()
        response.contentType = fileForOutput.getContentType()
    }


    def getAllImages() {
        println("Under+++++++++++getAllImages")
        def adid = params.id
        int width = 0
        int height = 0

        if (params.width)
            width = params.width as int
        //println("llll")
        if (params.height)
            height = params.height as int

        GridFSDBFile imageForOutput = gridfsService.getImages(adid, width, height);
        response.setContentType(imageForOutput.contentType)
        response.outputStream << imageForOutput?.inputStream?.bytes
    }

    def getPostImages() {
        GridFSDBFile imageForOutput = gridfsService.retrieveFile(params.id)
        response.setContentType(imageForOutput.getContentType())
        response.outputStream << imageForOutput.inputStream.bytes
    }

    def singleImage() {
        println("Id of image is+++++++++++="+params.id);
        GridFSDBFile imageForOutput = gridfsService.retrieveFile(params.id)
        response.setContentType(imageForOutput.contentType)
        response.outputStream << imageForOutput.inputStream.bytes
    }


    def save() {
        def adImageInstance = new AdImage(params)

        if (!adImageInstance.save(flush: true)) {
            render(view: "create", model: [adImageInstance: adImageInstance])
            return
        }
//		imageUploadService.save(adImageInstance)
        def file = request.getFile('logo')
        def imageTool = new ImageTool()
        imageTool.load(file.getBytes())
        imageTool.thumbnail(640)
        byte[] arr = imageTool.getBytes("JPEG")

        gridfsService.saveFile(arr, file.contentType, adImageInstance.id.toString() + "thumb.jpg")
        flash.message = message(code: 'default.created.message', args: [message(code: 'adImage.label', default: 'AdImage'), adImageInstance.id])
        redirect(action: "show", id: adImageInstance.id)
    }

    def show(Long id) {
        def adImageInstance = AdImage.get(id)
        if (!adImageInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'adImage.label', default: 'AdImage'), id])
            redirect(action: "list")
            return
        }

        [adImageInstance: adImageInstance]
    }

    def edit(Long id) {
        def adImageInstance = AdImage.get(id)
        if (!adImageInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'adImage.label', default: 'AdImage'), id])
            redirect(action: "list")
            return
        }

        [adImageInstance: adImageInstance]
    }

    def update(Long id, Long version) {
        def adImageInstance = AdImage.get(id)
        if (!adImageInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'adImage.label', default: 'AdImage'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (adImageInstance.version > version) {
                adImageInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                        [message(code: 'adImage.label', default: 'AdImage')] as Object[],
                        "Another user has updated this AdImage while you were editing")
                render(view: "edit", model: [adImageInstance: adImageInstance])
                return
            }
        }

        adImageInstance.properties = params

        if (!adImageInstance.save(flush: true)) {
            render(view: "edit", model: [adImageInstance: adImageInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'adImage.label', default: 'AdImage'), adImageInstance.id])
        redirect(action: "show", id: adImageInstance.id)
    }

    def delete(Long id) {
        def adImageInstance = AdImage.get(id)
        if (!adImageInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'adImage.label', default: 'AdImage'), id])
            redirect(action: "list")
            return
        }

        try {
            adImageInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'adImage.label', default: 'AdImage'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'adImage.label', default: 'AdImage'), id])
            redirect(action: "show", id: id)
        }
    }
}
